# Clem, the Clojure Error Mediator

Clem is a system for improving Clojure error messages, designed with beginners
in mind. Users can opt-in to Clem at a REPL. When an error occurs, Clem
intercepts the error, sends it to a web app, and gets a friendly explanation
back, assuming there is one, which it then shows to the user in place of the
default one. More experienced Clojure users can use the web app to create and
improve the friendly messages that are returned.

For more information on the repl integration, see the [Clem Repl project repo](https://gitlab.com/unc-app-lab/clem-repl/)

## So what is this thing?

Clem is a tool that integrates with users’ REPLs and helps provide more understandable error messages for new users. Probably most of you know that Clojure’s error messages are…unhelpful at times (how many beginners know that forgetting a quote on a list causes a [ClassCastException](https://github.com/yogthos/clojure-error-message-catalog/blob/master/clj/java-lang-string-cannot-be-cast-to-clojure-lang-ifn.md#error-message)?). This tool aims to bridge that gap for new users so that they can more quickly an easily get a foothold on the language.

This is accomplished using a REPL plugin/wrapper which reaches out to Clem’s server to get information on the exception, if it exists. This information is automatically printed in the user’s REPL, or if Clem doesn’t have a helpful message for that error, it prints Clojure’s normal message.

## How does it work

Clem overrides the exception-caught function of a repl and gathers some information about the exception which it sends to its server. Based on the stack-trace, message, type, and phase of the exception, Clem matches it to existing exceptions and returns a message which it then displays to the user. Everything is written in Clojure(Script) including the website!

Most users will be using [Clem's repl
interface](https://gitlab.com/unc-app-lab/clem-repl/) which automatically
fetches Clem's explanations for errors and prints them in the user's REPL, like
so:
![Example of Clem working in the terminal](images/repl-example.png)

For installation instructions see the [repl interface README](https://gitlab.com/unc-app-lab/clem-repl/)

Contributions are also welcome on the website. Each error has its own page where
information about the exception can be viewed and the friendly explanation for
the exception can be added.

![an exception page on Clem's website](images/clem-screen.png)

In order to edit an exception, log in using the profile icon on the top right,
select an exception, and then press the edit button to add an explanation.

## Contributing

### Providing Explanations

The simplest way to contribute to Clojure is to add explanations to exceptions
that are missing them in the database. You can do that by going to Clem's
website at [clem.applab.unc.edu](https://clem.applab.unc.edu) and signing in
using the profile button on the top right. From there you can click on exceptions
and edit their explanations.

### Testing

Any bugs found using either the website or [the REPL
interface](https://gitlab.com/unc-app-lab/clem-repl/) can be reported as issues
(either in this repo or the interface repo, honestly doesn't make a huge
difference). Please be sure to include the `clem.log` file generated by the REPL
interface if dealing with an issue there.

### Code Contributions

Merge Requests are most welcome! Clem use [a Trello
board](https://trello.com/b/y3bceH3w/clem-the-clojure-error-mediator) to manage
tasks. Take a look there and claim a task (either on Trello directly or by
opening an issue here), and start work! When finished, create a merge request
and make sure the Default reviewers are selected. One of the maintainers will
review your code, possibly offer suggestions, and then merge it once ready.

## Development Mode

### Setup Dev-local

Testing and local servers require [Datomic
dev-tools](https://docs.datomic.com/cloud/dev-local.html#getting-started). Please do any user-level setup
required before going further. Clem uses the tools.deps configuration, so follow
instructions for that.

Then install [AWS command line tools](https://aws.amazon.com/cli/) and configure
them in order to have access to the Datomic ions repo:

```
aws configure
```

### Start a system

A 'system' is everything that defines what a particular instance of this
application looks like as well as any global application state. At the moment,
that includes a web server process and a connection to a (possibly local and
faked) Datomic database server. This app uses the
[Integrant](https://github.com/weavejester/integrant) framework to configure,
check, start, and stop the system.

You can start the system from the command line or from the REPL. From the
command line, run `clojure -M:dev:run` to start a system that includes the
`wrap-reload` ring middleware to reload the source code with every request (to
be sure it's serving the latest code). 

You can also start the system from the REPL:

```
(require '[edu.unc.applab.clem.main :as main])
(def system (main/-main "-d"))
```

Removing a -d starts a production system, but this currently expects Datomic
ions, so won't work properly.

Now you can use the system, including transacting new facts to the database and
querying it. See the `comment` at the bottom of the `edu.unc.applab.clem.main`
namespace for an example.

To stop the system, say `(edu.unc.applab.clem.system/stop-system system)` from the REPL or hit control-c
from the command line.

### Compiling the frontend

In order to compile the frontend code to javascript, install
[shadow-cljs](https://shadow-cljs.github.io/docs/UsersGuide.html#_installation)
and in the project directory run:
```
shadow-cljs watch app
```

If a system is running (as above) it will automatically serve the frontend code
at whatever port the system is on

### Run tests

To run tests once:

```
./run-tests
```

in the project directory.

To start an auto-test runner run:
```
./run-tests -a
```

## Production build

A production system requires Datomic ions. If you're looking to run this system,
open an issue and let us know.
