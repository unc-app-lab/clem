#!/bin/bash

set -euxo pipefail

status_cmd=$(clojure -A:ion-dev '{:op :push}' 2>&1 | \
  grep -v ^Downloading | \
  grep -v '^(' | \
  clojure -e '
    (let [in (slurp *in*)
          _ (binding [*out* *err*] (println "\nOutput from push command:\n" in))
          {:keys [rev deploy-groups dependency-conflicts]} (read-string in)
          group (first deploy-groups)
          cmd (format "clojure %s '\''{:op :deploy, :group %s, :rev \"%s\"}'\''"
                      "-A:ion-dev" group rev)]
      (binding [*out* *err*] (println "\nAbout to execute command:\n" cmd))
      (println cmd))' | \
  bash | \
  clojure -e '
    (let [in (slurp *in*)
          _ (binding [*out* *err*] (println "\nOutput from deploy command:\n" in))
          {:keys [execution-arn]} (read-string in)
          cmd (format "clojure %s '\''{:op :deploy-status, :execution-arn %s}'\''"
                      "-A:ion-dev" execution-arn)]
      (println cmd))')

echo "Status command: $status_cmd"
if [ ! -z "$status_cmd" ]; then
  line=`echo $status_cmd | bash`
  set +x
  while true; do
    if [[ $line =~ "RUNNING" ]]; then
      echo "Deployment incomplete at $(date); sleeping 5 seconds."
    else
      break
    fi
    sleep 5
    line=`echo $status_cmd | bash`
  done
fi

echo "Deployment complete at $(date). Final deployment status: ${line}"
echo "$line" | \
clojure -e '(require '"'"'clojure.edn)
            (let [in (slurp *in*)
                  status (clojure.edn/read-string in)]
              (System/exit (if (or (not= "SUCCEEDED" (:deploy-status status))
                                   (not= "SUCCEEDED" (:code-deploy-status status)))
                               1
                               0)))'
