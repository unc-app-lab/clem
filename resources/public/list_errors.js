const EXPLANATION_INDEX = 1;
const VERSION_INDEX = 3;

const NO_VERSION_SELECTION = "Clojure Version";

const FILTER_EXPLANATION_ID = "filter-explanation";
const FILTER_NO_EXPLANATION_ID = "filter-no-explanation";

var tableRows = null;
var tableSortDir = null;
var tableSortNum = -1;

document.addEventListener('DOMContentLoaded', function() {
    var tbody = document.getElementById("error-table").querySelector("tbody");
    tableRows = Array.from(tbody.children);
}, false);

function sortTable(n, btn, dir = "asc", everSwitch = true) {
    // Function from: https://www.w3schools.com/howto/howto_js_sort_table.asp
    if (btn) {
        var sorterButtons = Array.from(document.querySelectorAll(".sorter"));
        sorterButtons.forEach((b) => b.classList.remove("selected"));
        btn.classList.add("selected");
    }

    // No actual sort specified
    if (n < 0) {
        return;
    }

    tableSortNum = n;
    tableSortDir = dir;

    var table, rows, switching, i, x, y, shouldSwitch, switchcount = 0;
    table = document.getElementById("error-table");
    switching = true;
    // Set the sorting direction to ascending:
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "asc" && everSwitch) {
                dir = "desc";
                tableSortDir = dir;
                switching = true;
            }
        }
    }
}

function filterTable(pred) {
    var versionFilter = document.getElementById('version-filter');
    var selectedVersion = versionFilter.options[versionFilter.selectedIndex].text;
    var table = document.getElementById("error-table");
    var newTbody = document.createElement("tbody");

    if (pred === true) {
        tableRows.forEach(row => newTbody.appendChild(row));
    } else {
        tableRows.forEach((tr) => {
            if (pred(selectedVersion, tr)) {
                newTbody.appendChild(tr);
            }
        });
    }
    table.replaceChild(newTbody, table.querySelector("tbody"));
    sortTable(tableSortNum, null, tableSortDir, false);
}

function shouldShowError(selected, tr) {
    var version = tr.children[VERSION_INDEX].textContent;
    if (selected !== version && selected !== NO_VERSION_SELECTION) {
        return false;
    }
    var expl = tr.children[EXPLANATION_INDEX];
    if (document.getElementById(FILTER_EXPLANATION_ID).classList.contains("selected")) {
        return Array.from(expl.children).some(td => td.classList.contains("yes"));
    } else if (document.getElementById(FILTER_NO_EXPLANATION_ID).classList.contains("selected")) {
        return Array.from(expl.children).some(td => td.classList.contains("no"));
    }

    return true;
}

function setFilterSelected(buttonSelected) {
    var otherBtn = buttonSelected === FILTER_EXPLANATION_ID ? FILTER_NO_EXPLANATION_ID : FILTER_EXPLANATION_ID;
    var explanationFilter = document.getElementById(buttonSelected);
    var explanationClasslist = explanationFilter.classList;
    if (explanationClasslist.contains("selected")) {
        explanationClasslist.remove("selected");
        return false;
    } else {
        document.getElementById(otherBtn).classList.remove("selected");
        explanationClasslist.add("selected");
        return true;
    }
}

function filterExplanation() {
    var filterEnabled = setFilterSelected(FILTER_EXPLANATION_ID);

    filterTable(filterEnabled ? shouldShowError : true);
}

function filterNoExplanation() {
    var filterEnabled = setFilterSelected(FILTER_NO_EXPLANATION_ID);

    filterTable(filterEnabled ? shouldShowError : true);
}

function filterVersion() {
    filterTable(shouldShowError);
}
