#! /bin/sh

adduser clemapp
mkdir /clem
mkdir /var/run/clem
chown clemapp:clemapp /clem /var/run/clem
exit 0 # This script always passes. Of course if its function fails something else down the line will too
