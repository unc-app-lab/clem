#! /bin/sh

cd $(dirname $0)/..
case "$1" in
    start)
        ARGS=`curl http://169.254.169.254/latest/user-data`
        /usr/local/bin/lein with-profile +aws trampoline run -- -p 3000 $ARGS > /clem/clem.log 2>&1 < /dev/null &
        echo $!>/var/run/clem/clem.pid
        ;;
    stop)
        if [ -e /var/run/clem/clem.pid ]; then
            kill `cat /var/run/clem/clem.pid` && rm /var/run/clem/clem.pid
        else
            exit 0
        fi
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    status)
        if [ -e /var/run/clem/clem.pid ]; then
            echo clem is running, pid=`cat /var/run/clem/clem.pid`
        else
            echo clem is NOT running
            exit 1
        fi
        ;;
    *)
        echo "Usage: $0 {start|stop|status|restart}"
esac

exit 0
