(ns edu.unc.applab.clem.db.exception-finding-spec
  (:require [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]
            [datomic.client.api :as d]
            [edu.unc.applab.clem.db.exception :as sut] ; subject under test
            [edu.unc.applab.clem.db.schema :as schema]
            [edu.unc.applab.clem.spec]
            [speclj.core :refer :all]
            [speclj.stub :refer [first-invocation-of invocations-of]]))

(s/def ::args*
  (s/or :leaf some?
        :node (s/coll-of ::args* :kind vector? :min-count 1)))
(s/def ::args (s/coll-of ::args* :kind vector? :min-count 1))

(s/def ::variable
  (s/with-gen symbol?
    #(gen/fmap (fn [sym] (symbol (str "?" sym)))
               (gen/symbol))))

(s/def ::in*
  (s/or :leaf ::variable
        :node (s/coll-of ::in* :kind vector? :min-count 1)))
(s/def ::in (s/coll-of ::in* :kind vector? :min-count 1))

(s/def ::rule
  (s/tuple ::variable keyword? ::variable))
(s/def ::clause
  (s/or :pred (s/tuple (s/coll-of symbol? :kind list? :count 3))
        :rule ::rule
        :func list?))
(s/def ::where
  (s/coll-of ::clause :min-count 1))

(s/def ::query
  (s/keys :req-un [::in ::where]))

(s/def ::subquery
  (s/keys :req-un [::args ::query]))

(def compile-exc-1 {:clojure-version "1.9.0"
                    :class "ClassCastException"
                    :message "Syntax error compiling at (C:/Users/tomy/IdeaProjects/clem-demo/deps.edn:5:3)"
                    :message-explanation "This is a second explanation"
                    :timestamp 1548710228726
                    :phase :compile-syntax-check
                    :stack-trace [{:class "clojure.ACastClass"
                                   :method "CastMethod"
                                   :file "CastFile"
                                   :line 15}
                                  {:class "BClass"
                                   :method "BMethod"
                                   :file "BFile"
                                   :line 4321}]
                    :caused-by nil})

(def compile-exc-2 {:clojure-version "1.9.0"
                     :class "ClassCastException"
                     :message "Syntax error compiling at (/Users/yolimeydan/Documents/COMP590_Class/src/Assignment1.clj:1:116)."
                     :message-explanation "This is a second explanation"
                     :timestamp 1548710228726
                     :phase :compile-syntax-check
                     :stack-trace [{:class "clojure.ACastClass"
                                    :method "CastMethod"
                                    :file "CastFile"
                                    :line 15}
                                   {:class "BClass"
                                    :method "BMethod"
                                    :file "BFile"
                                    :line 4321}]
                     :caused-by nil})

(def different-compile {:clojure-version "1.9.0"
                        :class "ClassCastException"
                        :message "	java.lang.RuntimeException: Can't take value of a macro: #'clojure.core/defn, compiling:(*cider-repl programming/clem-repl:localhost:43433(clj)*:1:7969)"
                        :message-explanation "This is a second explanation"
                        :timestamp 1548710228726
                        :phase :compile-syntax-check
                        :stack-trace [{:class "clojure.ACastClass"
                                       :method "CastMethod"
                                       :file "CastFile"
                                       :line 15}
                                      {:class "BClass"
                                       :method "BMethod"
                                       :file "BFile"
                                       :line 4321}]
                        :caused-by nil})

(describe "ste-subquery"
  (with exception-index 0)
  (with ste-index 1)

  (describe "with a STE without source info"
    (with ste {:class "clojure.AClass", :method "AMethod"})
    (with result (sut/ste-subquery @exception-index @ste-index @ste))

    (it "returns a subquery value"
      (should-be (partial s/valid? ::subquery) @result))

    (describe "the :args value"
      (it "contains exactly two items"
        (should= 2 (-> @result :args count)))

      (it "contains as its first item the class of the given STE"
        (should= (:class @ste) (-> @result :args first)))

      (it "contains as its second item the method of the given STE"
        (should= (:method @ste) (-> @result :args second))))

    (describe "the :in value"
      (with in (-> @result :query :in))

      (it "contains all symbols"
        (should-be #(every? symbol? %) @in))

      (it "prefixes all symbols with ?"
        (should-be #(every? (fn [sym] (.startsWith (str sym) "?")) %)
                   @in))

      (it "includes exception-index in all symbols"
        (should-be #(every? (fn [sym] (.contains (str sym)
                                                 (str "ex" @exception-index "-")))
                            %)
                   @in))

      (it "includes ste-index in all symbols"
        (should-be #(every? (fn [sym] (.contains (str sym)
                                                 (str "ste" @ste-index "-")))
                            %)
                   @in))

      (it "contains first a symbol for the class of the given STE"
        (should-be #(.contains % "-class") (str (first @in))))

      (it "contains second a symbol for the method of the given STE"
        (should-be #(.contains % "-method") (str (second @in)))))

    (describe "the :where value"
      (with where (-> @result :query :where))

      (it "contains exactly 5 items"
        (should= 5 (count @where)))

      (it "contains a rule for the STE class attribute"
        (should-contain
         '[?ex0-ste1-eid :stack-trace-element/class ?ex0-ste1-class-in]
         @where))

      (it "contains a rule for the STE method attribute"
        (should-contain
         '[?ex0-ste1-eid :stack-trace-element/method ?ex0-ste1-method-in]
         @where))

      (it "contains a rule to bind the STE index to a variable"
        (should-contain
         '[?ex0-ste1-eid :stack-trace-element/index ?ex0-ste1-index]
         @where))

      (it "contains a rule to connect the STE to its exception"
        (should-contain
         '[?ex0-eid :exception/stack-trace-element ?ex0-ste1-eid]
         @where))))

  (describe "with a STE with source info"
    (with ste {:class "clojure.AClass"
               :method "AMethod"
               :file "AFile"
               :line 1234})
    (with result (sut/ste-subquery @exception-index @ste-index @ste))

    (describe "the :args value"

      (it "contains as its third item the file of the given STE"
        (should= (:file @ste) (-> @result :args (nth 2))))

      (it "contains as its fourth item the line of the given STE"
        (should= (:line @ste) (-> @result :args (nth 3)))))

    (describe "the :in value"
      (with in (-> @result :query :in))

      (it "contains third a symbol for the file of the given STE"
        (should-be #(.contains % "-file") (str (nth @in 2))))

      (it "contains fourth a symbol for the line of the given STE"
        (should-be #(.contains % "-line") (str (nth @in 3)))))

    (describe "the :where value"
      (with where (-> @result :query :where))

      (it "contains a rule for the STE file attribute"
        (should-contain
         '[?ex0-ste1-eid :stack-trace-element/file ?ex0-ste1-file-in]
         @where))

      (it "contains a rule for the STE line attribute"
        (should-contain
         '[?ex0-ste1-eid :stack-trace-element/line ?ex0-ste1-line-in]
         @where))))

  (it "contains an index ordering predicate when the STE index is at least 1"
    (let [ste {:class "clojure.AClass", :method "AMethod"}
          ste-index 1
          result (sut/ste-subquery 0 ste-index ste)
          where (-> result :query :where)]
      (should (some #(list? (first %)) where))))

  (it "does not contain an index ordering predicate when the STE index is 0"
    (let [ste {:class "clojure.AClass", :method "AMethod"}
          ste-index 0
          result (sut/ste-subquery 0 ste-index ste)
          where (-> result :query :where)]
      (should (not-any? #(list? (first %)) where)))))

(describe "merge-subqueries, given 3 subquery maps as arguments"
  (with input-vec
    [{:args [1 2]
      :query {:in '[?ex0-ste0-class-in ?ex0-ste0-method-in]
              :where '[[(< ?foo ?bar)]]}}
     {:args [3 4]
      :query {:in '[?ex0-ste1-class-in ?ex0-ste1-method-in]
              :where '[[(< ?foo ?baz)]]}}
     {:args [5 6]
      :query {:in '[?ex0-ste2-class-in ?ex0-ste2-method-in]
              :where '[[(< ?foo ?bah)]]}}])

  (with result (sut/merge-subqueries @input-vec))

  (it "returns a subquery map"
    (should (s/valid? ::subquery @result)))

  (describe "the :args value"
    (it "includes the concatenated input :args values in order"
      (should= [1 2 3 4 5 6] (:args @result))))

  (describe "the :in value"
    (it "includes the concatenated input :in values in order"
      (should= '[?ex0-ste0-class-in ?ex0-ste0-method-in
                 ?ex0-ste1-class-in ?ex0-ste1-method-in
                 ?ex0-ste2-class-in ?ex0-ste2-method-in]
               (-> @result :query :in))))

  (describe "the :where value"
    (it "includes a concatenated sequence of the input :where values in order"
      (should= '[[(< ?foo ?bar)]
                 [(< ?foo ?baz)]
                 [(< ?foo ?bah)]]
               (-> @result :query :where)))))

(describe "stack-trace-subquery"
  (with-stubs)

  (with stack-trace
    [{:class "clojure.AClass", :method "AMethod"}
     {:class "clojure.BClass", :method "BMethod"}])

  (around [it]
          (with-redefs [sut/ste-subquery (stub :ste-subquery
                                               {:invoke (let [counter (atom 0)]
                                                          (fn [_ _ _]
                                                            (swap! counter inc)))})
                        sut/merge-subqueries (stub :merge-subqueries)
                        sut/filter-stack-trace (stub :filter-stack-trace
                                                     {:return @stack-trace})]
            (it)))

  (it "calls filter-stack-trace with the stack trace"
    (sut/stack-trace-subquery 0 @stack-trace)
    (should-have-invoked :filter-stack-trace {:with [@stack-trace]}))

  (it "calls ste-subquery for each filtered stack trace element"
    (sut/stack-trace-subquery 0 @stack-trace)
    (should-have-invoked :ste-subquery {:times (count @stack-trace)}))

  (it "passes the exception-index to calls to ste-subquery"
    (let [exception-index 0]
      (sut/stack-trace-subquery exception-index @stack-trace)
      (should (every? #(= exception-index (first %)) (invocations-of :ste-subquery)))))

  (it "sends different values for ste-index in each call to ste-subquery"
    (sut/stack-trace-subquery 0 @stack-trace)
    (should= (range (count @stack-trace)) (map second (invocations-of :ste-subquery))))

  (it "sends stack trace element as last argument in each call to ste-subquery"
    (sut/stack-trace-subquery 0 @stack-trace)
    (should= @stack-trace (map last (invocations-of :ste-subquery))))

  (it "calls merge-subqueries once"
    (sut/stack-trace-subquery 0 @stack-trace)
    (should-have-invoked :merge-subqueries {:times 1}))

  (it "calls merge-subqueries with the subqueries returned from calls to ste-subquery"
    (sut/stack-trace-subquery 0 @stack-trace)
    (should= [(range 1 (inc (count @stack-trace)))]
             (first-invocation-of :merge-subqueries))))

(describe "base-exception-subquery"
  (with exception
    {:class "java.RuntimeException"
     :clojure-version "1.10.0"
     :phase :execution
     :stack-trace
     [{:class "clojure.AClass", :method "AMethod"}
      {:class "clojure.BClass", :method "BMethod"}]})

  (with exception-index 0)

  (with result (sut/base-exception-subquery @exception @exception-index false))

  (describe "the :args value"

    (it "contains as the first value the clojure version of the exception"
      (should= (:clojure-version @exception) (-> @result :args first))))

  (describe "the :in value"
    (with in (-> @result :query :in))

    (it "uses the exception index in each value"
      (should (every? #(.startsWith (str %) (str "?ex" @exception-index)) @in)))

    (it "contains as the first value a variable for the clojure version"
      (should (.contains (str (first @in)) "clojure-version")))

    (it "contains as the second value a variable for the class"
      (should (.contains (str (second @in)) "class"))))

  (it "returns a subquery"
    (s/explain ::subquery @result)
    (should (s/valid? ::subquery @result))))

(describe "exception-query"
  (with-stubs)

  (with merged-result
    {:args [[[["clojure.AClass" "AMethod"]
              ["clojure.BClass" "BMethod"]]
             ["1.10.0" "exceptionClass"]]
            [[["clojure.CClass" "CMethod"]
              ["clojure.DClass" "DMethod"]]
             ["1.10.0" "exception2Class"]]]
     :query
     '{:in [[[[?ex0-ste0-class-in ?ex0-ste0-method-in]
              [?ex0-ste1-class-in ?ex0-ste1-method-in]]
             [?ex0-clojure-version-in ?ex0-class-in]]
            [[[?ex1-ste0-class-in ?ex1-ste0-method-in]
              [?ex1-ste1-class-in ?ex1-ste1-method-in]]
             [?ex1-clojure-version-in ?ex1-class-in]]]
       :where [[?ex0-ste0-eid :stack-trace-element/class ?ex0-ste0-class-in]
               [?ex0-ste0-eid :stack-trace-element/method ?ex0-ste0-method-in]
               [?ex0-ste0-eid :stack-trace-element/index ?ex0-ste0-index]
               [?ex0-ste1-eid :stack-trace-element/class ?ex0-ste1-class-in]
               [?ex0-ste1-eid :stack-trace-element/method ?ex0-ste1-method-in]
               [?ex0-ste1-eid :stack-trace-element/index ?ex0-ste1-index]
               [(< ?ex0-ste0-index ?ex0-ste1-index)]
               [?ex0-clojure-version-eid :clojure/version ?ex0-clojure-version-in]
               [?ex0-eid :exception/class ?ex0-class-in]
               [?ex0-eid :exception/clojure-version ?ex0-clojure-version-eid]
               [?ex1-ste0-eid :stack-trace-element/class ?ex1-ste0-class-in]
               [?ex1-ste0-eid :stack-trace-element/method ?ex1-ste0-method-in]
               [?ex1-ste0-eid :stack-trace-element/index ?ex1-ste0-index]
               [?ex1-ste1-eid :stack-trace-element/class ?ex1-ste1-class-in]
               [?ex1-ste1-eid :stack-trace-element/method ?ex1-ste1-method-in]
               [?ex1-ste1-eid :stack-trace-element/index ?ex1-ste1-index]
               [(< ?ex1-ste0-index ?ex1-ste1-index)]
               [?ex1-clojure-version-eid :clojure/version ?ex1-clojure-version-in]
               [?ex1-eid :exception/class ?ex1-class-in]
               [?ex1-eid :exception/clojure-version ?ex1-clojure-version-eid]]}})

  (around [it]
          (with-redefs [sut/stack-trace-subquery (stub :stack-trace-subquery
                                                       {:return :st-subquery})
                        sut/merge-subqueries (stub :merge-subqueries
                                                   {:return @merged-result})
                        sut/base-exception-subquery (stub :base-exception-subquery
                                                          {:return :base-subquery})]
            (it)))

  (with exception
    {:class "exceptionClass"
     :clojure-version "1.10.0"
     :stack-trace [{:class "clojure.AClass", :method "AMethod"}
                   {:class "clojure.BClass", :method "BMethod"}]
     :phase :execution
     :caused-by
     {:class "exception2Class"
      :clojure-version "1.10.0"
      :phase :execution
      :stack-trace [{:class "clojure.CClass", :method "CMethod"}
                    {:class "clojure.DClass", :method "DMethod"}]
      :caused-by nil}})

  (it "calls itself with a 0 index if no index is given"
    (let [self (stub :self)]
      (sut/exception-query :db @exception {} self)
      (should-have-invoked :self {:with [:db @exception {} 0 self]})))

  (it "calls stack-trace-subquery with the exception's stack trace"
    (sut/exception-query :db @exception)
    (should-have-invoked :stack-trace-subquery
                         {:with [0 (:stack-trace @exception)]}))

  (it "calls merge-subquery with the subquery results"
    (sut/exception-query :db @exception)
    (should-have-invoked :merge-subqueries
                         {:with [[:st-subquery :base-subquery]]}))

  (describe "when there is not a caused-by exception"
    (it "does not recur"
      (let [self (stub :self)]
        (sut/exception-query :db (dissoc @exception :caused-by) {} 0 self)
        (should-not-have-invoked :self))))

  (describe "the returned query value"

    (describe "when the exception-index is 0"
      (with result (sut/exception-query :db @exception {} 0 sut/exception-query))

      (it "includes a $ database variable in the :in part"
        (let [in (-> @result :query :in)]
          (should= '$ (first in))))

      (it "includes a variable for the root exception in the :find part"
        (let [find (-> @result :query :find)]
          (should= '[?ex0-eid] find)))

      (it "includes a database value in :args"
        (should= :db (-> @result :args first))))

    (describe "when the exception-index is not 0"
      (with result (sut/exception-query :db @exception {} 1 sut/exception-query))

      (it "does not include a $ database variable in the :in part"
        (let [in (-> @result :query :in)]
          (should-not= '$ (first in))))

      (it "does not include a variable for the root exception in the :find part"
        (let [find (-> @result :query :find)]
          (should-not= '[?ex0-eid] find)))

      (it "does not include a database value in :args"
        (should-not= :db (-> @result :args first))))))

(describe "find"
  (with exception {:clojure-version "20.23"
                   :class "clojure.3mcd"
                   :message "O59f10g7mSrV6"
                   :message-explanation nil
                   :phase :execution
                   :timestamp 12345678
                   :stack-trace
                   [{:class "javax.ypItUZ1W39Zsv142K18yp4"
                     :method "CL6pBQ4gX"
                     :file "fKgmTM8WFMa9BCABfVG"
                     :line 734550}
                    {:class "wa79Ll43e110i6o30"
                     :method "pC3F39R8xoey82e1B4A"
                     :file "i8Qm37ddAbunLLSB8lQf"
                     :line 11362}
                    {:class "java.itmC973SU2FaJ4hrcxdOB5v1Xt"
                     :method "5DZ6r7T4o361MUtMX6C11Bq"
                     :file "MAd653x19xBx0f2"
                     :line 35048}
                    {:class "YUsOU5Y0Px1"
                     :method "V854b51V7BnRzP8"
                     :file "FJCYT8KGCLo4tE82nrJe3Q20jBX"
                     :line 29354228}
                    {:class "clojure.2pAx"
                     :method "14AT0Tb40PPx5MT0"
                     :file "qQ3E0892"
                     :line 6}]
                   :caused-by
                   {:clojure-version "20.23"
                    :class "javax."
                    :message "1h9nPZQv5H0pDJjjxilf0j"
                    :message-explanation "HLW59TaZ6sK6by1KQeYeqKH13uU"
                    :phase :execution
                    :timestamp 12345678
                    :stack-trace
                    [{:class "hnwlurM869K22sB"
                      :method "s585v8EXFm6O0O6P7E3862ajcP1d"
                      :file "7fxe8mt9jT3o"
                      :line 52}
                     {:class "clojure.rp4HVF60kr4D1lGP"
                      :method "Wh8VNs4di1"
                      :file "Bm1EzOuB6VaE1zx"
                      :line 43}
                     {:class "javax.lby3uXC2"
                      :method "tx85Q1rO8z3of4X0iEgBb14ZcC1"
                      :file "O2MkPzu04JsPu"
                      :line 4117243}
                     {:class "java.JzZ2DWu4l4EK6S243Xk9ncFHVn6"
                      :method "bH40kNs1l14D2b7F86jB4qV7h"
                      :file "afile"
                      :line 2794}]
                    :caused-by nil}})

  (with exception2 {:clojure-version "1.9.0"
                    :class "RuntimeException"
                    :message "There was a problem."
                    :message-explanation "This is a explanation"
                    :timestamp 1548710213868
                    :phase :execution
                    :stack-trace [{:class "clojure.AClass"
                                   :method "AMethod"
                                   :file "AFile"
                                   :line 1234}
                                  {:class "BClass"
                                   :method "BMethod"
                                   :file "BFile"
                                   :line 4321}]
                    :caused-by {:clojure-version "1.9.0"
                                :class "RuntimeException"
                                :message "There was a deeper problem."
                                :message-explanation "A deeper explanation"
                                :phase :execution
                                :timestamp 1548710213868
                                :stack-trace [{:class "java.CClass"
                                               :method "CMethod"
                                               :file "CFile"
                                               :line 1122}]
                                :caused-by nil}})

  (with client (d/client {:server-type :dev-local
                          :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (before (sut/save! @conn @exception)
          (sut/save! @conn @exception2))

  (with id
    (ffirst (d/q '[:find ?e :in $ ?c :where [?e :exception/class ?c]]
                 (d/db @conn)
                 (:class @exception))))

  (with id2
    (ffirst (d/q '[:find ?e :in $ ?m :where [?e :exception/message ?m]]
                 (d/db @conn)
                 (:message @exception2))))

  (after (d/delete-database @client {:db-name "exc"}))

  (it "can find an exception"
    (let [result (sut/find (d/db @conn) @exception)]
      (should= @id result)))

  (it "returns nil when no exception exists"
    (let [result (sut/find (d/db @conn)
                           ;; just enough to make it different
                           (update @exception :class #(str % "-extra")))]
      (should= nil result)))

  (it "returns the proper exception, based on stacktrace"
    (let [result (sut/find (d/db @conn) @exception2)]
      (should= @id2 result)))

  (it "won't match an exception with a different phase"
      (let [saved-exc (sut/put! @conn @exception)
            new-phase-exc (assoc @exception :phase :macro-syntax-check)]
        (should-not= (:id saved-exc) (sut/find (d/db @conn) new-phase-exc))))

  (it "will match compile phase exceptions with similiar messages but different stack traces"
    (let [saved-exc (sut/put! @conn compile-exc-1)]
      (should= (:id saved-exc) (sut/find (d/db @conn) compile-exc-2))))

  (it "will not find compile phase exceptions with verry different messages"
    (let [saved-exc (sut/put! @conn compile-exc-1)]
      (should-not= (:id saved-exc) (sut/find (d/db @conn) different-compile)))))

(run-specs)
