(ns edu.unc.applab.clem.db.exception-spec
  (:require [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]
            [datomic.client.api :as d]
            [edu.unc.applab.clem.db.exception :as exc]
            [edu.unc.applab.clem.db.schema :as schema]
            [edu.unc.applab.clem.spec]
            [speclj.core :refer :all]))

(def form-for-exc #'edu.unc.applab.clem.db.exception/form-for-exc)

(defn- stack-trace= [orig result]
  (and
   (= (:class orig) (:stack-trace-element/class result))
   (= (:method orig) (:stack-trace-element/method result))
   (= (:file orig) (:stack-trace-element/file result))
   (= (:line orig) (:stack-trace-element/line result))))

(defn- exception-info= [orig result]
  (and

   (= (:class orig) (:exception/class result))
   (= (:message orig) (:exception/message result))
   (= (:timestamp orig) (:exception/timestamp result))
   (= (:phase orig) (:exception/phase result))
   (.startsWith (:exception/clojure-version result) "clj-version")))

(defn- clojure-version= [version result]
  (and (>= (count result) 4)
       (= (get result 3) version)))

(defn- exception-form= [orig result-set]
  (and (some #(exception-info= orig %) result-set)
       (some #(clojure-version= (:clojure-version orig) %) result-set)))

(defn- message-explanation= [orig result]
  (= (:message-explanation orig) (:message/explanation result)))

(defn- exception= [orig result]
  (and (= (dissoc orig :caused-by :caused-by-id :stack-trace :id) (dissoc result :caused-by :stack-trace :id :caused-by-id))
       (= (exc/filter-stack-trace (:stack-trace orig))
          (:stack-trace result))))

(def exception {:clojure-version "1.9.0"
                :class "RuntimeException"
                :message "There was a problem."
                :message-explanation "This is a explanation. Search-key"
                :timestamp 1548710213868
                :phase :execution
                :stack-trace [{:class "clojure.AClass"
                               :method "AMethod"
                               :file "AFile"
                               :line 1234}
                              {:class "BClass"
                               :method "BMethod"
                               :file "BFile"
                               :line 4321}]
                :caused-by {:clojure-version "1.9.0"
                            :class "RuntimeException"
                            :message "There was a deeper problem."
                            :message-explanation "A deeper explanation"
                            :phase :execution
                            :timestamp 1548710213868
                            :stack-trace [{:class "java.CClass"
                                           :method "CMethod"
                                           :file "CFile"
                                           :line 1122}]
                            :caused-by nil}})

(def exception2 {:clojure-version "1.9.0"
                 :class "ClassCastException"
                 :message "There was a casting problem. Search-key"
                 :message-explanation "This is a second explanation"
                 :timestamp 1548710228726
                 :phase :execution
                 :stack-trace [{:class "clojure.ACastClass"
                                :method "CastMethod"
                                :file "CastFile"
                                :line 15}
                               {:class "BClass"
                                :method "BMethod"
                                :file "BFile"
                                :line 4321}]
                 :caused-by nil})

(def exception3 {:clojure-version "1.10.0"
                 :class "ClassCastException"
                 :message "There a casting problem in a new version."
                 :message-explanation nil
                 :timestamp 1548710228726
                 :phase :execution
                 :stack-trace [{:class "clojure.CCastClass"
                                :method "CastMethod"
                                :file "CastFile"
                                :line 15}
                               {:class "BClass"
                                :method "AMethod"
                                :file "BFile"
                                :line 4321}]
                 :caused-by nil})

(describe "Saving Exceptions:"

  (with client (d/client {:server-type :dev-local
                          :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (with result (form-for-exc exception (d/db @conn)))

  (context "the result of form-for-exc"

    (it "is sequential"
      (should-be sequential? @result))

    (it "contains only vectors, seqs and maps"
      (should (every? #(or (vector? %)
                           (map? %)
                           (seq? %)) @result)))

    (it "contains a vector for the Clojure version"
      (should-contain [:db/add "clj-version0" :clojure/version "1.9.0"] @result))

    (it "does not contain a vector for the Clojure version if it already exists in the db"
      (exc/save! @conn exception)
      (should-not-contain [:db/add "clj-version0" :clojure/version "1.9.0"]
                          (form-for-exc exception2 (d/db @conn))))

    (it "contains a map for each stack trace element"
      ;; Uses this method of checking in order to not tie into the specific method of indexes used
      (should (some #(stack-trace=
                      {:class "clojure.AClass"
                       :method "AMethod"
                       :file "AFile"
                       :line 1234} %)
                    @result))
      (should (some #(stack-trace=
                      {:class "java.CClass"
                       :method "CMethod"
                       :file "CFile"
                       :line 1122} %)
                    @result)))

    (it "does not contain stack trace elements in the wrong groups."
      (should-not (some #(stack-trace=
                          {:class "BClass"
                           :method "BMethod"
                           :file "BFile"
                           :line 4321}
                          %)
                        @result)))

    (it "contains an entity map for the original exception"
      (should (exception-form= exception @result)))

    (it "contains an entity map for the caused-by exception"
      (should (exception-form= {:clojure-version "1.9.0"
                                :class "RuntimeException"
                                :message "There was a deeper problem."
                                :message-explanation "A deeper explanation"
                                :phase :execution
                                :timestamp 1548710213868
                                :stack-trace [{:class "java.CClass"
                                               :method "CMethod"
                                               :file "CFile"
                                               :line 1122}]
                                :caused-by nil}
                               @result)))

    (it "contains a map for the message explanation"
      (should (some #(message-explanation= % exception) @result))))

  (context "directly saving an exception to the database"
    (it "should save with no errors"
      (exc/save! @conn exception))

    (it "should contain a single exception with the passed query"
      (exc/save! @conn exception)
      (should= 1 (count (d/q '[:find ?e :where [?e :exception/class "RuntimeException"]
                               [?e :exception/message "There was a problem."]] (d/db @conn)))))

    (it "should throw no errors when two exceptions with the same clojure version are added"
      (exc/save! @conn exception)
      (exc/save! @conn exception2))

    (it "should throw an error when an entity id caused-by map is passed."
      (should-throw AssertionError (exc/save! @conn (assoc exception :caused-by-id 33))))))

(describe "Fetching exceptions:"

  (with client (d/client {:server-type :dev-local
                          :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (edu.unc.applab.clem.db.schema/install-schema! conn)
          conn))

  (before (exc/save! @conn exception))

  (after (d/delete-database @client {:db-name "exc"}))

  (with id (ffirst (d/q [:find '?e :where ['?e :exception/message (:message exception)]] (d/db @conn))))

  (with db (d/db @conn))

  (context "the result of calling fetch"

    (it "creates an exception equal to the added one"
      ;; :id removed as this is not a stable part of the exception
      (let [fetched-exception (-> (exc/fetch @db @id)
                                  (dissoc :id :caused-by-id)
                                  (assoc :caused-by nil))]
        (should (exception= exception fetched-exception))))

    (it "returns an exception valid to the spec"
      (should-be (partial s/valid? :edu.unc.applab.clem.spec/found-exception) (exc/fetch @db @id)))

    (it "contains an id equal to the entity id"
      (should= @id (:id (exc/fetch @db @id))))

    (it "is nil for an invalid id"
      (should-be-nil (exc/fetch @db 33)))

    (it "does not contain unnecessary keys"
      (let [result (exc/fetch @db @id)
            str-keys (map str (keys result))]
        (should-not-contain #":exception/.*" str-keys)
        (should-not-contain :message/explanation result)
        (should-not-contain :clojure/version result)))

    (it "returns nil when passed nil"
      (should-be-nil (exc/fetch @db nil)))))

(describe "Finding caused exceptions:"

  (def caused-exc
    {:clojure-version "2.28.28-Co25lqtS-SNAPSHOT",
     :class "javax.GCl382Ogd05g",
     :message "1",
     :message-explanation "2i690P8P6XfGCg2",
     :phase :execution
     :timestamp 2860526,
     :stack-trace
     [{:class "javax.4jE",
       :method "102V5JV1ose1s2q2sMwdAY36bb",
       :file "4JlxSv1J3C",
       :line 3}
      {:class "java.a78tomj",
       :method "2o4uE3K8K8SZ72prwQclI2",
       :file "ogws81yvIQv2sv042Hb4s7Ds648a",
       :line 43791590}
      {:class "XtHo",
       :method "9U55WuMVH8Dx141X30h",
       :file "5834CtU029M5YGEDfH23T7awMfVf",
       :line 2089260}
      {:class "clojure.gYFZ",
       :method "6R3pDdVSf2D7slV",
       :file "oeiW2F42Ye4a1a",
       :line 18270}],
     :caused-by
     {:clojure-version "2.28.28-Co25lqtS-SNAPSHOT",
      :class "clojure.579lUSz97cvj2t4xx0hLZmg",
      :message "a2bdvWkzVtglMsG838zU3aqWZXxU6",
      :message-explanation "Lm6w9j2SY",
      :phase :execution
      :timestamp 110273,
      :stack-trace
      [{:class "javax.1E4l38",
        :method "atazanb0gv7e0nK1zx7uU",
        :file "PsflVq",
        :line 121275}
       {:class "java.eAT4r717Lz1I6CfCTK1v5zZbKUaK06",
        :method "6tQA779qR33m",
        :file "tC1F8cV",
        :line 1477}]
      :caused-by nil,
      :id 5957336}})

  (with client (d/client {:server-type :dev-local
                          :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (before (exc/save! @conn caused-exc))

  (after (d/delete-database @client {:db-name "exc"}))

  (with causing-id (ffirst (d/q '[:find ?e :in $ ?c :where [?e :exception/class ?c]] (d/db @conn) (get-in caused-exc [:caused-by :class]))))

  (with caused-id (ffirst (d/q '[:find ?e :in $ ?c :where [?e :exception/class ?c]] (d/db @conn) (:class caused-exc))))

  (it "returns all causing-ids"
    (should= #{@caused-id} (exc/find-caused (d/db @conn) @causing-id)))

  (it "returns an empty set on an invalid id"
    (should= #{} (exc/find-caused (d/db @conn) 1)))

  (it "returns multiple values with multiple caused exceptions"
    (exc/save! @conn exception2)
    (let [exc2-id (ffirst (d/q '[:find ?e :in $ ?c :where [?e :exception/class ?c]] (d/db @conn) (:class exception2)))]
      ;;This sets up two exceptions with the same causing function, something that this
      ;;function handles but doesn't currently occur naturally.
      (d/transact @conn {:tx-data [[:db/add exc2-id :exception/caused-by @causing-id]]})
      (should= #{@caused-id exc2-id} (exc/find-caused (d/db @conn) @causing-id))))

  (it "returns an empty set on an exception that causes no exceptions"
    (should= #{} (exc/find-caused (d/db @conn) @caused-id))))

(describe "List Exceptions:"

  (with client (d/client {:server-type :dev-local
                          :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (it "should list all exceptions"
    (exc/save! @conn exception)
    (exc/save! @conn exception2)
    (let [excs (exc/list (d/db @conn))]
      (should (some (partial exception= exception) excs))
      (should (some (partial exception= exception2) excs))
      ;; Include caused-by exceptions saved
      (should (some (partial exception= (:caused-by exception)) excs))))

  (it "should return an empty list if no exceptions"
    (should-be empty? (exc/list (d/db @conn))))

  (it "should not keep more than 100 exceptions by default"
    (doall (map (partial exc/save! @conn) (gen/sample (s/gen :edu.unc.applab.clem.spec/exception) 105)))
    (should= 100 (count (exc/list (d/db @conn)))))

  (it "should return different exceptions for different start and end values"
    (doall (map (partial exc/save! @conn) (gen/sample (s/gen :edu.unc.applab.clem.spec/exception) 30)))
    (let [excs-1 (exc/list (d/db @conn) {:start 0 :end 10})
          excs-2 (exc/list (d/db @conn) {:start 11 :end 21})]
      (should-not (some #(some #{%} excs-2) excs-1))))

  (it "should return only exceptions with occurences if specified"
    (let [{exc-id :id} (exc/put! @conn exception)
          {exc2-id :id} (exc/put! @conn exception2)]
      (exc/increment-occurrences! @conn exc-id)
      (let [excs (exc/list (d/db @conn) {:min-occurences 1})]
        (should (some (partial exception= exception) excs))
        (should= 1 (count excs)))))

  (it "should return the right number of exceptions"
    (doall (map (partial exc/save! @conn) (gen/sample (s/gen :edu.unc.applab.clem.spec/exception) 10)))
    (should= 10 (count (exc/list (d/db @conn) {:end 10})))))


(describe "put!"
  (with client (d/client {:server-type :dev-local
                          :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (with complex-exception
    {:clojure-version "19.23",
     :class "clojure.3X8M0Erm57veUrA2X3g",
     :message "3C79806veqmF7s1iTAwIl5B4F",
     :message-explanation "8yx2U",
     :phase :execution
     :timestamp 9997754,
     :stack-trace
     [{:class "bTgo3FLF229HSH6lY6jv",
       :method "ky2l5S2",
       :file "W4Q8",
       :line 4177}],
     :caused-by
     {:clojure-version "19.23",
      :class "javax.vOykKwsD6tJ1myPUxl8Btv",
      :message "8E7wRk3b7VHKYH",
      :message-explanation nil,
      :phase :execution
      :timestamp 1621,
      :stack-trace
      [{:class "Gvg5Wn5ylHD24o720onb2",
        :method "6HWAXdZ070r34So8O8OIQt7p7"}
       {:class "clojure.XfT57v0BFb9N6Ahvcp7zL",
        :method "lkNP6O7jDE61QwXN50I"}
       {:class "clojure.94LV6x5Z5XV12", :method "XX0GRWSN"}
       {:class "javax.JWebVO87qW1KO0BN2hMpl55W",
        :method "WKx1dUScw3z9SAG678k5FY8j96NMHp"}
       {:class "clojure.8Uu24kxp7d8mGBWnvks", :method "4xhmVVjW33G1PPTr"}],
      :caused-by
      {:clojure-version "19.23",
       :class "bTUL15iw",
       :message "C",
       :message-explanation "07I1MTHM",
       :phase :execution
       :timestamp 534326619,
       :stack-trace
       [{:class "clojure.AiX4KTdRq80C25RGdP1Sx8O9be38",
         :method "62v91ImKnlQ8rJ86"}
        {:class "javax.IF4GYh",
         :method "k9SDzi8W5QibFpbppH",
         :file "egImHE0mMYBRyfV",
         :line 1720738}
        {:class "javax.JE30q7T6743BSb1Eb8rROy2iK2G8", :method "F6Zng"}
        {:class "clojure.dDbPIPNsb7NYV8",
         :method "gYZs8nm0wrbZ6a7Kcht2oL3"}
        {:class "clojure.l51m4452", :method "5ZSZvh55i6w4A89b40"}
        {:class "javax.neGFNk93k54lYudP72Tbedj",
         :method "K593a01GLHKB2p7rKjX"}
        {:class "clojure.Gjqo9OZ2bcf1K5MA9oY86A4v68z7", :method "t"}],
       :caused-by
       {:clojure-version "19.23",
        :phase :execution
        :class "382",
        :message "rNUH5JGqmk4vMDCgm20EK",
        :message-explanation "4tx",
        :timestamp 16372005,
        :stack-trace
        [{:class "clojure.WU554XcecK",
          :method "FVk66pMQsu46kUzKC7QdveSqKfO",
          :file "8cwKV781E6B5X84y5n9",
          :line 18}
         {:class "clojure.Ar3mm9ehH40M38",
          :method "I1tJSRE8h7la5TUNk01c67",
          :file "6R5oWY7u1",
          :line 66}
         {:class "javax.6QT0x8wnyhHTcXa7po",
          :method "l0fBAUGYNH7Ws2R",
          :file "C3iE101PpJ5hI5Mm3hP129663Sxd5",
          :line 107}
         {:class "clojure.X1Bx33a9rTE64eL3ViqovkVE2x9",
          :method "F2QHI0qg17f2GCj7Rb4py40Yy",
          :file "1P",
          :line 2}
         {:class "clojure.px0cAVO6H84D3HNPKvMIz",
          :method "Z3Nyn64",
          :file "5MVUdx3A6z60Hv2NOU88",
          :line 48146}
         {:class "FA7a44UFFv67Tf121d4", :method "8yW4tY2sLfKh"}
         {:class "clojure.0WUg5Ir6oYV03B8W3Ap", :method "w8euW"}
         {:class "clojure.P", :method "x6Ww4RjB6766X9Fn"}
         {:class "v1i1iMLKD28IZCC2D", :method "Rk0ydw4JYy43Sen53"}
         {:class "yES7rOw64e3",
          :method "2I9uCZv1q38nU",
          :file "TGyR22t4z0A7Ua1",
          :line 34669}
         {:class "clojure.Mo56UykO9tTkLQ53Y89Xrr",
          :method "io5Tgw073sB35GCIDY5TR",
          :file "4845yzfOpD77Bii0pJ6O5Y9",
          :line 1}
         {:class "javax.onXEdwsO23xNy6Zz216U9dd181",
          :method "5Pv9oRW4u77Rj3Q0m8",
          :file "C0qy071vk4UVEi19mSptZ",
          :line 31566576}
         {:class "javax.sx9wK7cl4SohJEc6cc", :method "b9hzDsqaOi8"}
         {:class "clojure.6",
          :method "Md89d0c",
          :file "YWh2Si2592zTt6vY3",
          :line 27413}
         {:class "zFfaCOO", :method "C4l9"}
         {:class "javax.J5IwZ9KO81ekT",
          :method "l94ZMtNI9499FuDIAYIqCm9h117s09",
          :file "5jE5T2P0f5HC3W3RL",
          :line 69}
         {:class "Gc65E32vw", :method "K5pHo8tOP"}
         {:class "java.2no29NzV3X3HG19tq8WZCI0AI3",
          :method "2RI7Y93fO80I3z7dfbHkao2hPZ51H",
          :file "Pr53fkRc0IlGeJSFsHt6Z",
          :line 64972361}
         {:class "javax.1Y4631kV526m9q1Fgck6", :method "9zt1"}
         {:class "java.OUzl1FHEd", :method "jwZcck0V4UPF26r5"}],
        :caused-by
        {:clojure-version "19.23",
         :class "javax.S4",
         :phase :execution
         :message "2r0t3lg",
         :message-explanation "oPmx48d7TeEVXL4t1k1rNr6c4Fu06G",
         :timestamp 192,
         :stack-trace
         [{:class "javax.iO15p5I905Hc79F2xCmoX",
           :method "4TduVm7cWP0X9TEERXFozlHX05q",
           :file "Tb6scTpBc0oSb069ll",
           :line 3466549}
          {:class "java.FuGtF9Vd271KDZMj9Sa7CJNXF", :method "HsF9lI9"}
          {:class "java.DbPLdG74xUSYiXJYg7tj",
           :method "m84242rzn8pSSV1oKQHK",
           :file "h2qAgZMWzd1iM",
           :line 2336070}
          {:class "u1YrF2QluD4sY", :method "jI3bwjA90Q0qk636"}],
         :caused-by
         {:clojure-version "19.23",
          :phase :execution
          :class "javax.z4ijp8VTFrtnY4rEU4HEsrS2BkW",
          :message "Q4134033261g9m",
          :message-explanation "908aa0",
          :timestamp 685740,
          :stack-trace
          [{:class "52SJ5L73B9X1MYKC6z", :method "N772TO95R5SwQ6N80"}
           {:class "P6zi2mb58Iy3FX8",
            :method "1NbGxZ7WuWx4PzlO9g27o8FDiqtFxt"}
           {:class "8Qgpka224214uee34bl4w70N7UYH", :method "jmS4OW69"}],
          :caused-by
          {:clojure-version "19.23",
           :phase :execution
           :class "java.8Rov5xdS3",
           :message "2izY6E7cNJZ0AI19CbeyDohwkgd98",
           :message-explanation nil,
           :timestamp 19919,
           :stack-trace
           [{:class "java.313Dr9bo5FDIa2C",
             :method "s3YhnNDYEtDJlm9TEi6s"}],
           :caused-by
           {:clojure-version "19.23",
            :class "h2tG12d",
            :message "49OlPX2aTw4osaK",
            :message-explanation "8s1Y",
            :timestamp 317,
            :phase :execution
            :stack-trace
            [{:class "clojure.BaSh715z1OORgXX9LOmNNFNyV",
              :method "yb3wBANH0as5G7OzVGsmIX49Iw"}
             {:class "java.u",
              :method "8X9MGT85A",
              :file "5z9UIG1uqPY2dJ3Xr6z71",
              :line 2}
             {:class "clojure.u7cCVP0hclq", :method "05wLih1ki0GCbTz0P"}
             {:class "clojure.bqDzV01XPf8v2V7u66HPz7z",
              :method "L8wvtQyZj",
              :file "11u9uTzV3sW2nUfgSCKjR",
              :line 113}
             {:class "QcA61J93b", :method "3uakv"}],
            :caused-by
            {:clojure-version "19.23",
             :phase :execution
             :class "clojure.AZ",
             :message "2zC75Y3YXAn",
             :message-explanation "05PQD7OdbXD48494nKQE",
             :timestamp 1049695,
             :stack-trace
             [{:class "8SyBG6pVE33sHt7590t",
               :method "1d84m7f7EfHkqa2V6RoKCOA"}
              {:class "javax.I7znK27jk5Cpjk853du4uZjQ",
               :method "30",
               :file "goXyqMn6hZa6310aTRXWKEvRfDPS0",
               :line 186043070}
              {:class "java.9sn2xE04a6tW0",
               :method "6Pa3H172LB",
               :file "2WqM6s20ha4aC6Lf",
               :line 2}
              {:class "java.uP8IYIs6HxuQt38GTU",
               :method "hb9YC5X2dOb53PLj5JVXAxpOO42"}
              {:class "clojure.h9cV0i2Klg79Nk5xMaFN",
               :method "d96l6O8qfh",
               :file "G5WTMAS15hAc0l4mM5B24Ic818vX",
               :line 3}
              {:class "javax.nq4w1j7jc",
               :method "GMU6177FKO60Sq5OD5sKTLXFIJ1q",
               :file "aURF1H819",
               :line 38604462}
              {:class "java.HBm8ERK3S", :method "H2qUIf"}
              {:class "javax.i8HHq52ckFqU2SU952s", :method "5iO51"}],
             :caused-by
             {:clojure-version "19.23",
              :phase :execution
              :class "",
              :message "oT25jwcmh5J2YcA",
              :message-explanation "",
              :timestamp 15262487,
              :stack-trace
              [{:class "", :method "h9jP990dUNHVbPV"}
               {:class "238N1pVeR2wD84Qo3DyG59M7W7am",
                :method "e3y81Z8UT8DlZb86dZ154Ec3",
                :file "29SByIA0gdMUj",
                :line 12474158}
               {:class "javax.NaZ93BiE3KDV83c",
                :method "2v2w32k",
                :file "tZ29ZsTFefg1rlzI38271YOl7",
                :line 9006}
               {:class "BC",
                :method "99M6go4BMa5Z8H3MPA1A743674",
                :file "BV9Ct8ID4XVB7TJ94a55vkt7djgzRS",
                :line 2168218}
               {:class "fbK9075Fj8M3fi398Z7hHMLJogqkf",
                :method "283CbIXnN6tcZgOYxtZ93",
                :file "LZmjxoh2cJ90wh74v564Tu3n0",
                :line 6861}
               {:class "java.ktViPg",
                :method "Fy23n",
                :file "fmd8m371xRFq3wrCUW348T",
                :line 256}
               {:class "clojure.4U1ny82y50", :method "jz"}],
              :caused-by nil,
              :id 217855715},
             :id 2542621}},
           :id 15}},
         :id 781},
        :id 2644840}}},
     :id 5387985})

  (it "puts an exception in the database if it does not already exists."
    (exc/put! @conn exception)
    (should-not-be-nil (exc/find (d/db @conn) exception)))

  (context "if a similar exception already exists"

    (it "does not put an exception in the database"
      (exc/save! @conn exception)
      (let [old-cnt (count (exc/list (d/db @conn)))]
        (exc/put! @conn exception)
        (should= old-cnt (count (exc/list (d/db @conn))))))

    (it "does not put an exception in the database (with different message)"
      (exc/save! @conn exception)
      (let [old-cnt (count (exc/list (d/db @conn)))]
        (exc/put! @conn (update exception :message #(str % "-new")))
        (should= old-cnt (count (exc/list (d/db @conn))))))

    (it "returns an exception with the passed exception's stacktrace and message"
      (exc/save! @conn exception)
      (let [sim-exc (-> exception
                        (update :message #(str % " (also similar)"))
                        (update :stack-trace #(conj % {:class "rando.SimClass"
                                                       :method "SimMethod"
                                                       :file "SimFile"
                                                       :line 437})))
            result (exc/put! @conn sim-exc)]
        (should= (dissoc exception :caused-by :caused-by-id :id :message :stack-trace)
                 (dissoc result :caused-by :caused-by-id :id :message :stack-trace))
        (should= (:message sim-exc) (:message result))
        (should= (:stack-trace sim-exc) (:stack-trace result)))))

  (it "should ignore id if it is specified"
    (exc/put! @conn (assoc exception :id 300))
    (should-not-be-nil (exc/find (d/db @conn) exception)))

  (it "returns the passed exception if it is not in the database"
    (let [saved-exc (exc/put! @conn exception)]
      (should-contain :id saved-exc)
      (should= (dissoc exception :caused-by :id :caused-by-id)
               (dissoc saved-exc :caused-by :id :caused-by-id))))

  (it "should not return an exception with an empty id"
    (should-not-be-nil (:id (exc/put! @conn exception))))

  (it "doesn't re-add matched causing exceptions"
    (exc/save! @conn (:caused-by exception))
    (let [c-id (exc/find (d/db @conn) (:caused-by exception))
          result (exc/put! @conn exception)]
      (should= c-id (:caused-by-id result))))

  (it "creates a proper caused-by relationship in the database."
    (let [saved-exc (exc/put! @conn exception)]
      (should= (:caused-by-id saved-exc)
               (:caused-by-id (exc/fetch (d/db @conn) (:id saved-exc))))))

  (it "successfully saves a complex exception"
    (let [saved-exc (exc/put! @conn @complex-exception)]
      (should-contain :id saved-exc)
      (should= (dissoc @complex-exception :caused-by :id :caused-by-id)
               (dissoc saved-exc :caused-by :id :caused-by-id))))

  (describe "use an inherited explanation"

    (it "when the incoming exception exists"
      (exc/put! @conn exception)
      (exc/put! @conn exception)
      (let [newer-exception (assoc exception :clojure-version "1.10.1"
                                   :message-explanation nil)]
        (exc/put! @conn newer-exception)
        (should= (:message-explanation exception) (:message-explanation
                                                         (exc/put! @conn newer-exception)))))

    (it "when the incoming exception does not exist"
      (exc/put! @conn exception)
      (let [newer-exception (assoc exception :clojure-version "1.10.1"
                                   :message-explanation nil)]
        (should= (:message-explanation exception) (:message-explanation
                                                         (exc/put! @conn newer-exception)))))))

(describe "Fetch Exceptions:"
  (with client (d/client {:server-type :dev-local
                          :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (with exception-num 15)

  (after (d/delete-database @client {:db-name "exc"}))

  (before (exc/save! @conn exception)
          (d/transact @conn {:tx-data [[:db/add @id :exception/occurences @exception-num]]}))

  (with id (ffirst (d/q [:find '?e :where ['?e :exception/message (:message exception)]] (d/db @conn))))
  (with db (d/db @conn))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))
  (it "correctly retrieves the number of exception occurences for a given exception"
    (let [occurences-num (exc/fetch-occurences @db @id)]
      (should= @exception-num occurences-num)))
  (it "returns zero because the exception id does not exist"
    (let [fake-id 879878797980
          occurences-num (exc/fetch-occurences @db fake-id)]
      (should= 0 occurences-num))))

(describe "Increment Occurrences"

  (with client (d/client {:server-type :dev-local
                          :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (it "increments a new exception"
    (let [excid (:id (exc/put! @conn exception))]
      (exc/increment-occurrences! @conn excid)
      (should= 1 (exc/fetch-occurences (d/db @conn) excid))))

  (it "increments multiple times"
    (let [excid (:id (exc/put! @conn exception))]
      (exc/increment-occurrences! @conn excid)
      (exc/increment-occurrences! @conn excid)
      (exc/increment-occurrences! @conn excid)
      (should= 3 (exc/fetch-occurences (d/db @conn) excid)))))

(describe "Fetch occurrence listing"
  (with client (d/client {:server-type :dev-local
                          :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (it "returns proper occurrence counts"
    (let [excid (:id (exc/put! @conn exception))
          excid2 (:id (exc/put! @conn exception2))
          n 3
          n2 2]
      (dotimes [_ n]
        (exc/increment-occurrences! @conn excid))
      (dotimes [_ n2]
        (exc/increment-occurrences! @conn excid2))
      (let [result (exc/fetch-occurence-listing (d/db @conn) [excid excid2])]
        (should= n (get result excid))
        (should= n2 (get result excid2)))))

  (it "returns an empty collection when passed an empty collection"
    (should-be empty? (exc/fetch-occurence-listing (d/db @conn) []))))

(describe "Fetch all Clojure versions"

  (with client (d/client {:server-type :dev-local
                          :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (it "returns all versions"
    (exc/save! @conn exception)
    (exc/save! @conn exception3)
    (should== ["1.10.0" "1.9.0"] (exc/fetch-clojure-versions (d/db @conn))))

  (it "returns empty with no exceptions"
    (should= [] (exc/fetch-clojure-versions (d/db @conn))))

  (it "returns only one copy of a version, even if multiple instances exist in the db"
    (exc/save! @conn exception)
    (exc/save! @conn exception2)
    (should= ["1.9.0"] (exc/fetch-clojure-versions (d/db @conn)))))

(describe "exception-count"

  (with client (d/client {:server-type :dev-local
                          :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (it "returns the count of exceptions"
    (doall (->> (gen/sample (s/gen :edu.unc.applab.clem.spec/exception) 10)
                (map #(assoc % :caused-by nil))
                (map (partial exc/save! @conn))))
    (should= 10 (exc/exception-count (d/db @conn))))

  (it "counts the correct number when given a min-occurrences argument"
    (let [{exc-id :id} (exc/put! @conn exception)
          {exc2-id :id} (exc/put! @conn exception2)]
      (exc/increment-occurrences! @conn exc-id)
      (should= 1 (exc/exception-count (d/db @conn) 1))))

  (it "returns 0 on an empty db"
    (should= 0 (exc/exception-count (d/db @conn)))))

(describe "search"
  (with client (d/client {:server-type :dev-local
                          :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))
  (it "returns an empty array in an empty db"
    (should (empty? (exc/search (d/db @conn) ["a strings"]))))

  (it "returns exception ids for exceptions containing the string"
    (let [{exc-id :id} (exc/put! @conn exception)
          {exc-id2 :id} (exc/put! @conn exception2)]
      (should= #{exc-id exc-id2} (set (exc/search (d/db @conn) ["Search-key"])))))

  (it "returns an empty sequence when no exception matches"
    (exc/put! @conn exception)
    (exc/put! @conn exception)
    (should= [] (exc/search (d/db @conn) ["You will never find this search key"])))

  (it "matches all search keys"
    (let [{exc-id :id} (exc/put! @conn exception)
          {exc-id2 :id} (exc/put! @conn exception2)
          {exc-id3 :id} (exc/put! @conn exception3)]
      (should= #{exc-id2 exc-id3} (set (exc/search (d/db @conn) ["CastMethod" "problem"]))))))

(describe "inherited-explanation"

  (with client (d/client {:server-type :dev-local
                          :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (describe "when an older exception exists"

    (it "returns nil if the older exception doesn't have an explanation"
      (exc/put! @conn (assoc exception :message-explanation nil))
      (let [newer-exception (assoc exception :clojure-version "1.10.1"
                                   :message-explanation nil)]
        (should-be-nil (exc/inherited-explanation (d/db @conn) newer-exception))))

    (it "returns newest option of available explanations"
      (exc/put! @conn (assoc exception :message-explanation nil))
      (let [newer-exception (assoc exception :clojure-version "1.10.0"
                                   :message-explanation "new message")
            newest-exception (assoc exception :clojure-version "1.10.1"
                                    :message-explanation nil)]
        (exc/put! @conn newer-exception)
        (should= "new message" (exc/inherited-explanation (d/db @conn) newest-exception))))

    (it "returns the message an exception with an older version"
      (exc/put! @conn exception)
      (let [newer-exception (assoc exception :clojure-version "1.10.1"
                                   :message-explanation nil)]

        (should= (:message-explanation exception) (exc/inherited-explanation (d/db @conn) newer-exception))))

    (it "doesn't return the message for an older exception"
      (exc/put! @conn exception)
      (let [older-exception (assoc exception :clojure-version "1.8.1"
                                   :message-explanation nil)]
        (should-be-nil (exc/inherited-explanation (d/db @conn) older-exception)))))

  (it "returns nil if no matching exceptions are in the database"
    (let [newer-exception (assoc exception :clojure-version "1.10.1"
                                 :message-explanation nil)]
      (should-be-nil (exc/inherited-explanation (d/db @conn) newer-exception)))))

(run-specs)
