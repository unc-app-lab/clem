(ns edu.unc.applab.clem.db.explanation-spec
  (:require [datomic.client.api :as d]
            [edu.unc.applab.clem.db.exception :as exc]
            [edu.unc.applab.clem.db.explanation :as expl]
            [clojure.spec.alpha :as s]
            [edu.unc.applab.clem.db.schema :as schema]
            [edu.unc.applab.clem.spec]
            [speclj.core :refer :all]))

(def exception {:clojure-version "1.9.0"
                :class "RuntimeException"
                :message "There was a problem."
                :message-explanation "This is a explanation"
                :timestamp 1548710213868
                :phase :execution
                :stack-trace [{:class "clojure.AClass"
                               :method "AMethod"
                               :file "AFile"
                               :line 1234}
                              {:class "BClass"
                               :method "BMethod"
                               :file "BFile"
                               :line 4321}]
                :caused-by {:clojure-version "1.9.0"
                            :class "RuntimeException"
                            :message "There was a deeper problem."
                            :message-explanation "A deeper explanation"
                            :timestamp 1548710213868
                            :phase :execution
                            :stack-trace [{:class "java.CClass"
                                           :method "CMethod"
                                           :file "CFile"
                                           :line 1122}]
                            :caused-by nil}})

(def example-user "aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee")

(describe "Saving explanations:"
  (with client (d/client {:server-type :dev-local
 :system "clem-test"}))
  (with conn (d/create-database @client {:db-name "exc"})
                (let [conn (d/connect @client {:db-name "exc"})]
                  (schema/install-schema! conn)
                  conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (it "should transact correctly"
      (exc/save! @conn exception)
      (let [eid (ffirst (d/q '[:find ?e
                                :in $ ?message
                                :where [?e :exception/message ?message]] (d/db @conn) (:message exception)))
            explanation {:exception-id eid :explanation-string "this is a completely diff string"}]
        (expl/save! @conn explanation example-user)
        (should= (:explanation-string explanation)
                 (ffirst (d/q '[:find ?message
                        :in $ ?e
                        :where [?e :exception/message-explanation ?message-ref]
                               [?message-ref :message/explanation ?message]]
                      (d/db @conn) eid)))))

  (it "should return nil when exception ids are not found in the database"
      (let [explanation {:exception-id 342453 :explanation-string "this should not transact"}]
        (should-be-nil (expl/save! @conn explanation example-user))))

  (it "should properly record the saved user"
    (exc/save! @conn exception)
    (let [eid (ffirst (d/q '[:find ?e
                             :in $ ?message
                             :where [?e :exception/message ?message]] (d/db @conn) (:message exception)))
          explanation {:exception-id eid :explanation-string "this is a completely diff string"}]
      (expl/save! @conn explanation example-user)
      (should= example-user
               (ffirst (d/q '[:find ?user
                              :in $ ?e
                              :where [?e :exception/message-explanation ?message-ref]
                              [?message-ref :message/user ?user]]
                            (d/db @conn) eid))))))

(describe "Getting Explanation History"

  (with client (d/client {:server-type :dev-local
 :system "clem-test"}))
  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (before (exc/save! @conn exception))
  (after (d/delete-database @client {:db-name "exc"}))

  (with eid (ffirst (d/q '[:find ?e
                          :in $ ?message
                          :where [?e :exception/message ?message]] (d/db @conn) (:message exception))))

  (it "should return an collection with the newest first"
    (expl/save! @conn {:exception-id @eid :explanation-string "1"} example-user)
    (expl/save! @conn {:exception-id @eid :explanation-string "2"} example-user)
    (expl/save! @conn {:exception-id @eid :explanation-string "3"} example-user)
    (should= ["3" "2" "1" "This is a explanation"] (expl/history (d/db @conn) @eid)))

  (it "returns an empty vector if there is no history"
    (let [local-eid (:id (exc/put! @conn
                                   {:clojure-version "1.9.0"
                                    :class "NotTheSameRuntimeException"
                                    :message "You may not have an explanation for this"
                                    :message-explanation nil
                                    :phase :execution
                                    :timestamp 1548710213868
                                    :stack-trace [{:class "clojure.AClass"
                                                   :method "AMethod"
                                                   :file "AFile"
                                                   :line 1234}
                                                  {:class "BClass"
                                                   :method "BMethod"
                                                   :file "BFile"
                                                   :line 4321}]
                                    :caused-by nil}))]
      (should= [] (expl/history (d/db @conn) local-eid))))

  (it "shows duplicate values multiple times"
    (expl/save! @conn {:exception-id @eid :explanation-string "3"} example-user)
    (expl/save! @conn {:exception-id @eid :explanation-string "2"} example-user)
    (expl/save! @conn {:exception-id @eid :explanation-string "3"} example-user)
    (should= ["3" "2" "3" "This is a explanation"] (expl/history (d/db @conn) @eid))))

(run-specs)
