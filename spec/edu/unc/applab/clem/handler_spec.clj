(ns edu.unc.applab.clem.handler-spec
  (:require [speclj.core :refer :all]
            [edu.unc.applab.clem.handler :as h]
            [edu.unc.applab.clem.db.exception :as exc]
            [datomic.client.api :as d]
            [edu.unc.applab.clem.db.schema :as schema]
            [clojure.edn :as edn]))

(def exception {:clojure-version "1.9.0"
                :class "RuntimeException"
                :message "There was a problem."
                :message-explanation "This is a explanation"
                :timestamp 1548710213868
                :phase :execution
                :stack-trace [{:class "clojure.AClass"
                               :method "AMethod"
                               :file "AFile"
                               :line 1234}
                              {:class "BClass"
                               :method "BMethod"
                               :file "BFile"
                               :line 4321}]
                :caused-by {:clojure-version "1.9.0"
                            :class "RuntimeException"
                            :message "There was a deeper problem."
                            :message-explanation "A deeper explanation"
                            :timestamp 1548710213868
                            :phase :execution
                            :stack-trace [{:class "java.CClass"
                                           :method "CMethod"
                                           :file "CFile"
                                           :line 1122}]
                            :caused-by nil}})

(def exc-to-send (h/convert-id-to-string exception))

(describe "Converting ids to strings"

  (it "should convert numerically ids to strings"
    (should= {:clojure-version "1.9.0"
              :id "33"
              :class "RuntimeException"
              :message "There was a problem."
              :message-explanation "This is a explanation"
              :timestamp 1548710213868
              :phase :execution
              :stack-trace [{:class "clojure.AClass"
                             :method "AMethod"
                             :file "AFile"
                             :line 1234}
                            {:class "BClass"
                             :method "BMethod"
                             :file "BFile"
                             :line 4321}]
              :caused-by {:clojure-version "1.9.0"
                          :class "RuntimeException"
                          :message "There was a deeper problem."
                          :message-explanation "A deeper explanation"
                          :timestamp 1548710213868
                          :phase :execution
                          :stack-trace [{:class "java.CClass"
                                         :method "CMethod"
                                         :file "CFile"
                                         :line 1122}]
                          :caused-by nil}}
             (h/convert-id-to-string (assoc exception :id 33))))

  (it "shouldn't add any keys"
    (should= exception (h/convert-id-to-string exception)))

  (it "should also adjust caused-by"
    (should=
     {:clojure-version "1.9.0"
      :class "RuntimeException"
      :message "There was a problem."
      :message-explanation "This is a explanation"
      :phase :execution
      :timestamp 1548710213868
      :stack-trace [{:class "clojure.AClass"
                     :method "AMethod"
                     :file "AFile"
                     :line 1234}
                    {:class "BClass"
                     :method "BMethod"
                     :file "BFile"
                     :line 4321}]
      :caused-by {:clojure-version "1.9.0"
                  :id "55"
                  :class "RuntimeException"
                  :message "There was a deeper problem."
                  :message-explanation "A deeper explanation"
                  :timestamp 1548710213868
                  :phase :execution
                  :stack-trace [{:class "java.CClass"
                                 :method "CMethod"
                                 :file "CFile"
                                 :line 1122}]
                  :caused-by nil}}
     (h/convert-id-to-string (assoc-in exception [:caused-by :id] 55))))

  (it "should also update caused-by-id"
    (should=
     {:clojure-version "1.9.0"
      :id "33"
      :class "RuntimeException"
      :message "There was a problem."
      :message-explanation "This is a explanation"
      :phase :execution
      :timestamp 1548710213868
      :stack-trace [{:class "clojure.AClass"
                     :method "AMethod"
                     :file "AFile"
                     :line 1234}
                    {:class "BClass"
                     :method "BMethod"
                     :file "BFile"
                     :line 4321}]
      :caused-by-id "55"}
     (h/convert-id-to-string {:clojure-version "1.9.0"
                              :id 33
                              :class "RuntimeException"
                              :message "There was a problem."
                              :message-explanation "This is a explanation"
                              :timestamp 1548710213868
                              :phase :execution
                              :stack-trace [{:class "clojure.AClass"
                                             :method "AMethod"
                                             :file "AFile"
                                             :line 1234}
                                            {:class "BClass"
                                             :method "BMethod"
                                             :file "BFile"
                                             :line 4321}]
                              :caused-by-id 55}))))

(describe "Convert strings to ids"

  (it "should convert numerically ids to strings"
    (should= {:clojure-version "1.9.0"
              :id 33
              :class "RuntimeException"
              :message "There was a problem."
              :message-explanation "This is a explanation"
              :timestamp 1548710213868
              :phase :execution
              :stack-trace [{:class "clojure.AClass"
                             :method "AMethod"
                             :file "AFile"
                             :line 1234}
                            {:class "BClass"
                             :method "BMethod"
                             :file "BFile"
                             :line 4321}]
              :caused-by {:clojure-version "1.9.0"
                          :class "RuntimeException"
                          :message "There was a deeper problem."
                          :message-explanation "A deeper explanation"
                          :timestamp 1548710213868
                          :stack-trace [{:class "java.CClass"
                                         :method "CMethod"
                                         :file "CFile"
                                         :line 1122}]
                          :phase :execution
                          :caused-by nil}}
             (h/convert-string-to-id (assoc exception :id "33"))))

  (it "shouldn't add any keys"
    (should= exception (h/convert-string-to-id exception)))

  (it "should also adjust caused-by"
    (should=
     {:clojure-version "1.9.0"
      :class "RuntimeException"
      :message "There was a problem."
      :message-explanation "This is a explanation"
      :timestamp 1548710213868
      :phase :execution
      :stack-trace [{:class "clojure.AClass"
                     :method "AMethod"
                     :file "AFile"
                     :line 1234}
                    {:class "BClass"
                     :method "BMethod"
                     :file "BFile"
                     :line 4321}]
      :caused-by {:clojure-version "1.9.0"
                  :id 55
                  :class "RuntimeException"
                  :message "There was a deeper problem."
                  :message-explanation "A deeper explanation"
                  :timestamp 1548710213868
                  :phase :execution
                  :stack-trace [{:class "java.CClass"
                                 :method "CMethod"
                                 :file "CFile"
                                 :line 1122}]
                  :caused-by nil}}
     (h/convert-string-to-id (assoc-in exception [:caused-by :id] "55"))))

  (it "should also update caused-by-id"
    (should=
     {:clojure-version "1.9.0"
      :id 33
      :class "RuntimeException"
      :message "There was a problem."
      :message-explanation "This is a explanation"
      :timestamp 1548710213868
      :phase :execution
      :stack-trace [{:class "clojure.AClass"
                     :method "AMethod"
                     :file "AFile"
                     :line 1234}
                    {:class "BClass"
                     :method "BMethod"
                     :file "BFile"
                     :line 4321}]
      :caused-by-id 55}
     (h/convert-string-to-id {:clojure-version "1.9.0"
                              :id "33"
                              :class "RuntimeException"
                              :message "There was a problem."
                              :message-explanation "This is a explanation"
                              :timestamp 1548710213868
                              :phase :execution
                              :stack-trace [{:class "clojure.AClass"
                                             :method "AMethod"
                                             :file "AFile"
                                             :line 1234}
                                            {:class "BClass"
                                             :method "BMethod"
                                             :file "BFile"
                                             :line 4321}]
                              :caused-by-id "55"})))
  )

(describe "List Errors"
  (with client (d/client {:server-type :dev-local
 :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (before (exc/save! @conn exception))

  (it "returns a 200 status"
    (should= 200 (:status (h/handler {:request-method :get
                                      :uri "/api/errors"
                                      :datomic/conn @conn}))))

  (it "returns a 400 status if a bad parameter is given"
    (should= 400 (:status (h/routes {:request-method :get
                                     :params {:start "ab"}
                                     :uri "/api/errors"
                                     :datomic/conn @conn}))))

  (it "has an application/edn content type"
    (should= "application/edn" (:content-type (h/handler {:request-method :get
                                                          :uri "/api/errors"
                                                          :datomic/conn @conn})))))

(describe "Get error"

  (with client (d/client {:server-type :dev-local
 :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (before (exc/save! @conn exception))

  (with id
    (ffirst (d/q '[:find ?e :in $ ?c :where [?e :exception/message ?c]]
                 (d/db @conn)
                 (:message exception))))

  (it "returns a 200 status for a valid exception"
    (should= 200 (:status (h/handler {:request-method :get
                                      :uri (str "/api/error/" @id)
                                      :datomic/conn @conn}))))

  (it "has a content-type of application/edn"
    (should= "application/edn" (:content-type (h/handler {:request-method :get
                                                          :uri (str "/api/error/" @id)
                                                          :datomic/conn @conn}))))

  (it "returns 404 on a non-existent exception"
    (should= 404 (:status (h/handler {:request-method :get
                                      :uri "/api/error/33"
                                      :datomic/conn @conn})))))

(describe "Explanation upsert"
  (with client (d/client {:server-type :dev-local
 :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (before (exc/save! @conn exception))

  (with id
    (ffirst (d/q '[:find ?e :in $ ?c :where [?e :exception/message ?c]]
                 (d/db @conn)
                 (:message exception))))

  (it "returns a 200 to the proper exception when given valid terms."
    (let [resp (h/handler {:request-method :post
                           :uri "/api/explanation"
                           :session {:identity {:username "name" :sub "aaaa-bbbb-cccc-dddd"}}
                           :datomic/conn @conn
                           :params {"exception-id" (str @id),
                                    "explanation-string" "test"}})]
      (should= 200 (:status resp))
      (should= "test" (ffirst (d/q '[:find ?m
                                     :in $ ?id
                                     :where [?id :exception/message-explanation ?me]
                                     [?me :message/explanation ?m]]
                                   (d/db @conn)
                                   @id)))))

  (it "returns a 400 status on a malformed example"
    (should= 400 (:status (h/handler {:request-method :post
                                      :uri "/api/explanation"
                                      :session {:identity {:username "name" :sub "aaaa-bbbb-cccc-dddd"}}
                                      :datomic/conn @conn
                                      :params {"exception-id" "123a",
                                               "explanation-string" "cool"}}))))

  (it "returns a 400 status on a parameterless example"
    (should= 400 (:status (h/handler {:request-method :post
                                      :uri "/api/explanation"
                                      :session {:identity {:username "name" :sub "aaaa-bbbb-cccc-dddd"}}
                                      :datomic/conn @conn}))))

  (it "returns a 404 status for a nonexistent exception-id"
    (should= 404 (:status (h/handler {:request-method :post
                                      :uri "/api/explanation"
                                      :session {:identity {:username "name" :sub "aaaa-bbbb-cccc-dddd"}}
                                      :datomic/conn @conn
                                      :params {"exception-id" "33",
                                               "explanation-string" "test"}}))))

  (it "returns a 401 status if the user is not logged in"
    (should= 401 (:status (h/handler {:request-method :post
                                      :uri "/api/explanation"
                                      :datomic/conn @conn
                                      :params {"exception-id" (str @id),
                                               "explanation-string" "test"}})))))

(describe "Put Error endpoint"
  (with client (d/client {:server-type :dev-local
 :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (it "returns a 201 when a new exception is added to the database"
    (should= 201 (:status (h/handler {:request-method :put
                                      :uri "/api/error"
                                      :datomic/conn @conn
                                      :content-type "application/edn"
                                      :body (pr-str exc-to-send)}))))

  (it "returns a 200 when an exception already in the database is returned."
    (exc/put! @conn exception)
    (should= 200 (:status (h/handler {:request-method :put
                                      :uri "/api/error"
                                      :datomic/conn @conn
                                      :content-type "application/edn"
                                      :body (pr-str exc-to-send)}))))

  (it "returns a 400 when bad exception data is given"
    (should= 400 (:status (h/handler {:request-method :put
                                      :uri "/api/error"
                                      :datomic/conn @conn
                                      :content-type "application/edn"
                                      :body (pr-str
                                             (dissoc exc-to-send :message))}))))

  (it "returns a 415 if content type is wrong"
    (should= 415 (:status (h/handler {:request-method :put
                                      :uri "/api/error"
                                      :datomic/conn @conn
                                      :content-type "application/text"
                                      :body (pr-str exc-to-send)}))))

  (it "returns 400 if edn is malformed"
    (should= 400 (:status (h/handler {:request-method :put
                                      :uri "/api/error"
                                      :datomic/conn @conn
                                      :content-type "application/edn"
                                      :body "{:not-edn"}))))

  (it "increments associated exception occurrences"
    (let [excmap (exc/put! @conn exception)
          current-occurences (exc/fetch-occurences (d/db @conn) (:id excmap))]
      (h/handler {:request-method :put
                  :uri "/api/error"
                  :datomic/conn @conn
                  :content-type "application/edn"
                  :body (pr-str exc-to-send)})
      (should= (inc current-occurences) (exc/fetch-occurences (d/db @conn) (:id excmap)))))

  (it "does not increment if an error occurs during call"
    (let [excmap (exc/put! @conn exception)
          current-occurences (exc/fetch-occurences (d/db @conn) (:id excmap))]
      (h/handler {:request-method :put
                  :uri "/api/error"
                  :datomic/conn @conn
                  :content-type "application/edn"
                  :body "{:not-edn"})
      (should= current-occurences (exc/fetch-occurences (d/db @conn) (:id excmap)))))

  (it "increments occurrences from 0 to 1 on newly found exceptions"
    (let [resp (h/handler {:request-method :put
                           :uri "/api/error"
                           :datomic/conn @conn
                           :content-type "application/edn"
                           :body (pr-str exc-to-send)})]
      (should= 1 (exc/fetch-occurences (d/db @conn) (:id (h/convert-string-to-id (edn/read-string (:body resp)))))))))

(describe "Getting Ocurrences"

  (with client (d/client {:server-type :dev-local
 :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (before (exc/save! @conn exception))

  (with id (ffirst (d/q [:find '?e :where ['?e :exception/message (:message exception)]] (d/db @conn))))

  (it "returns 200 for a valid exception"
    (= 200 (:status (h/handler {:request-method get
                                :uri "/api/occurrences"
                                :datomic/conn @conn
                                :content-type "application/edn"
                                :body (pr-str [@id])}))))

  (it "returns 400 for malformed edn"
    (= 400 (:status (h/handler {:request-method get
                                :uri "/api/occurrences"
                                :datomic/conn @conn
                                :content-type "application/edn"
                                :body "{:not-edn"}))))

  (it "returns 415 if content-type is not edn"
    (= 415 (:status (h/handler {:request-method get
                                :uri "/api/occurrences"
                                :datomic/conn @conn
                                :content-type "application/json"
                                :body (pr-str [@id])})))))

(describe "Get Versions"
  (with client (d/client {:server-type :dev-local
 :system "clem-test"}))

  (with conn (d/create-database @client {:db-name "exc"})
        (let [conn (d/connect @client {:db-name "exc"})]
          (schema/install-schema! conn)
          conn))

  (after (d/delete-database @client {:db-name "exc"}))

  (before (exc/save! @conn exception))

  (it "returns a 200 status"
    (should= 200 (:status (h/handler {:request-method :get
                                      :uri "/api/versions"
                                      :datomic/conn @conn}))))

  (it "Returns a list of versions"
    (should= ["1.9.0"] (edn/read-string (:body (h/handler {:request-method :get
                                                           :uri "/api/versions"
                                                           :datomic/conn @conn}))))))
(describe "Matching Whitelist"
  (it "always fails on an empty list"
    (should-not (h/matches-whitelist [] "A string")))

  (it "returns true with a matching url"
    (should (h/matches-whitelist ["http://clem.com", "http://clem.org"] "http://clem.com/a/path")))

  (it "returns false with a non-matching url"
    (should-not (h/matches-whitelist ["http://clem.org"] "http://clem.com/a/path")))

  (it "returns false if the whitelist doesn't start with the passed url"
    (should-not (h/matches-whitelist ["http://clem.orghttp://clem.com"] "http://clem.com/a/path")))

  (it "returns false on a nil url"
    (should-not (h/matches-whitelist ["http://clem.com", "http://clem.org"] nil))))

(run-specs)
