(ns edu.unc.applab.clem.auth
  (:require [buddy.core.keys :as keys]
            [buddy.sign.jwt :as jwt]
            [buddy.sign.jws :as jws]
            [cheshire.core :as ch]
            [clj-http.client :as http]
            [clojure.string :as string]
            [clojure.spec.alpha :as s])
  (:import java.util.Base64))


(s/def ::jwt (s/tuple string? string? string?))

(defn get-header [jwt]
  (let [vals (string/split jwt #"\.")
        decoder (Base64/getDecoder)]
    (if (s/valid? ::jwt vals)
      (ch/parse-string (String. (.decode decoder (first vals))) true)
      (throw (IllegalArgumentException. (s/explain-str ::jwt vals))))))

(defn get-payload [jwks jwt]
  (let [header (get-header jwt)
        jwk (first (filter #(= (:kid %) (:kid header)) jwks))]
    (if jwk
      (->
       (jws/unsign jwt (keys/jwk->public-key jwk) {:alg (keyword (.toLowerCase (:alg header)))})
       String.
       (ch/parse-string true))
      (throw (IllegalStateException. (str "No matching jwk for key " (:kid header)))))))

(defn decode-jwt! [jwt region user-pool-id]
  (let [jwks (get-in (http/get (str "https://cognito-idp." region ".amazonaws.com/" user-pool-id "/.well-known/jwks.json") {:as :json}) [:body :keys])]
    (get-payload jwks jwt)))
