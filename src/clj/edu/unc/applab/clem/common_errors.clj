(ns edu.unc.applab.clem.common-errors
  (:require clojure.main
            [edu.unc.applab.clem.repl :refer [error->map]]))

(defn get-error-data [form]
  (try
    (eval form)
    (catch Throwable error
      (error->map error))))

(defn common-errors []
  (for [x ['a
           'a/b
           'user/a
           '(let [a] a)
           '(defn a (+ 1 1))
           '(for [] 1)
           '(for [i] (* i i))
           '@a
           '(var 7)]]
    (get-error-data x)))

(defn print-usage-and-exit [exit-code]
  (println
    (str "Usage: lein run -m edu.unc.applab.clem.common-errors [-h | --help]
   or: lein run -m edu.unc.applab.clem.common-errors [- | <outfile>]
Specific Actions:
-h, --help    Prints usage string
-             Prints the error data instead of saving to a file
<outfile>     Saves the error data to this file\n"))
  (System/exit exit-code))

(defn -main [& args]
  (count args)
  (if (not= (count args) 1)
    (print-usage-and-exit 1)
    (let [arg (first args)]
      (if (or (= arg "-h")
              (= arg "--help"))
        (print-usage-and-exit 0)
        (let [error-string (apply str (mapv prn-str (common-errors)))]
          (if (= arg "-")
            (print error-string)
            (spit arg error-string)))))))
