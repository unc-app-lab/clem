(ns edu.unc.applab.clem.config
  (:require [datomic.ion :as ion]))
(def cli-options
  "Defines the options that can be passed into Clem. While this format is for commandline
  options, the same options can be set with environment variables. In environement
  variables the long name of the option is used as keyword. Ex. to set the PORT option
  provide a map with {:port 3001}.

  See also create-environment"
  [["-p" "--port PORT" "Port number"
    :default 3000
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ["-d" "--devel" "Marks server as devel"]
   ["-u" "--sign-in-url URL" "The URL to use for sign-up/sign-in"]
   ["-c" "--client-id ID" "Amazon Cognito client id"]
   ["-a" "--auth-region REGION" "Region for the AWS Cognito userpool. Defaults to the same as region"]
   ["-i" "--user-pool-id ID" "Amazon Cognito user pool id"]
   ["-r" "--region REGION" "Datomic AWS region"]
   ["-s" "--system SYSTEM" "Datomic system name"]
   ["-e" "--endpoint ENDPOINT" "Datomic endpoint"]
   ["-w" "--redirect-whitelist WHITELIST" "Comma separated list of valid URLS to redirect to"]
   ["-C" "--cookie-secret SECRET" "Secret to be used in creating cookie store."]])

(defn create-environment
  "Determines various parameters based on two sources: command-line options and
  datomic environment variables. If a key exists in both sources, then command-line
  arguments take precedence. See cli-options for possible options

  In an ions environment, the datomic environment variables are set in the CloudFormation
  settings. In other contexts it is read from the DATOMIC_ENV_MAP environment variable.
  A map in EDN format is expected.

  parsed-args is the commandline arguments already parsed by parse-opts"
  [parsed-args]
  (merge
   (ion/get-env)
   (get parsed-args :options)))

(defn generate-cognito-config
  [{:keys [sign-in-url client-id auth-region user-pool-id redirect-whitelist
           region] :as options}]
  {:sign-in sign-in-url
   :client-id client-id
   :region (or auth-region region)
   :user-pool-id user-pool-id
   :whitelist (.split redirect-whitelist, ",")})
