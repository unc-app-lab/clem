(ns edu.unc.applab.clem.db.exception
  (:refer-clojure :exclude [find list])
  (:require [clojure.spec.alpha :as s]
            [datomic.client.api :as d]
            [edu.unc.applab.clem.spec :as clem-spec]
            [version-clj.core]
            [edu.unc.applab.clem.db.query-functions])) ; Query functions not directly used but should be required

(def min-jaccard-index 0.6)

(defn fetch-occurences [db eid]
  (let [eid-occurence-query '[:find ?occurences
                             :in $ ?e
                             :where [?e :exception/occurences ?occurences]]
        results (d/q eid-occurence-query db eid)]
        (if (not-empty results) (ffirst results) 0)))

(defn- assoc-if-not-nil
  [m k v]
  (if v
    (assoc m k v)
    m))

(defn- gen-stack-trace-form [exc-idx ste-idx stk-tr]
  (-> {:db/id (str "stack-trace-" exc-idx "-" ste-idx)
       :stack-trace-element/index ste-idx
       :stack-trace-element/class (:class stk-tr)
       :stack-trace-element/method (:method stk-tr)}
      (assoc-if-not-nil :stack-trace-element/file (:file stk-tr))
      (assoc-if-not-nil :stack-trace-element/line (:line stk-tr))))

(defn- gen-exception-base-form [index new-index stack-trace exception version-form caused-by-id?]
  (let [base-map {:db/id (str "exception" index)
                  :exception/occurences 0
                  :exception/clojure-version version-form
                  :exception/class (:class exception)
                  :exception/message (:message exception)
                  :exception/timestamp (:timestamp exception)
                  :exception/phase (:phase exception)
                  :exception/stack-trace-element (map #(str "stack-trace-" index "-" %) (range (count stack-trace)))}
        base-map (if (:message-explanation exception)
                   (assoc base-map :exception/message-explanation (str "explanation" index))
                   base-map)]
    (if (and (not caused-by-id?) (:caused-by exception))
      (assoc base-map :exception/caused-by (str "exception" new-index))
      (if (and caused-by-id? (:caused-by-id exception))
        (assoc base-map :exception/caused-by (:caused-by-id exception))
        base-map))))

(defn- gen-message-explanation [index exception]
  {:db/id (str "explanation" index)
   :message/explanation (:message-explanation exception)})

(defn- validate-exception-form [exception]
  (assert (s/valid? ::clem-spec/exception exception)
          (str "save! only accepts a full exception map.\n"
               "The output of clojure.spec.alpha/explain-str follows: "
               (s/explain-str ::clem-spec/exception (:caused-by exception))))
  (assert (not (contains? exception :caused-by-id))
          (str "save! only accepts a full exception map.\n"
               "You passed it a map with a `:caused-by-id` field, like what is\n"
               "returned from edu.unc.applab.clem.db.exception/fetch.\n"
               "Although I could save the top-level exception anyway, I'm\n"
               "cowardly refusing to do it because you probably want to save\n"
               "the full caused-by chain as well, and this won't get you\n"
               "there. If this is what you want, please dissoc :caused-by-id\n"
               "and try again.\n")))

(defn filter-stack-trace
  [stack-trace]
  (letfn [(predicate [element]
            (or (.startsWith (:class element) "clojure.")
                (.startsWith (:class element) "java.")
                (.startsWith (:class element) "javax.")))]
    (filterv predicate stack-trace)))

(defn- form-for-exc
  ([exception db]
   (form-for-exc exception db 0 false))
  ([exception db caused-by-id?]
   (form-for-exc exception db 0 caused-by-id?))
  ([exception db index caused-by-id?]
   (let [stack-trace (filter-stack-trace (:stack-trace exception))
         version-in-db (not (empty? (d/q '[:find ?e :in $ ?k ?v :where [?e ?k ?v]]
                                    db :clojure/version (:clojure-version exception))))
         new-index (inc index)]

     (concat (if-not caused-by-id?
               (if-let [new-exc (:caused-by exception)]
                 (form-for-exc new-exc db new-index caused-by-id?)))
             (map-indexed #(gen-stack-trace-form index %1 %2) stack-trace)
             (if version-in-db
               []
               [[:db/add (str "clj-version" index) :clojure/version (:clojure-version exception)]])
             (if (:message-explanation exception)
               [(gen-message-explanation index exception)]
               [])
             [(gen-exception-base-form index new-index stack-trace exception
                                       (if version-in-db
                                         [:clojure/version (:clojure-version exception)]
                                         (str "clj-version" index))
                                       caused-by-id?)]))))

(comment
  (require '[clojure.spec.alpha :as s]
           '[clojure.spec.gen.alpha :as gen]
           '[edu.unc.applab.clem.spec :as clms])
  (def exc (assoc (gen/generate (s/gen ::clem-spec/exception))
                  :caused-by nil)))

(defn save!
  "Saves an exception to the database. Expects an exception conforming to
  edu.unc.applab.clem.spec/exception

  This will also recursively save all the exceptions in the :caused-by key of the passed
  exception."
  [conn exception]
  (validate-exception-form exception)
  (d/transact conn {:tx-data (form-for-exc exception (d/db conn))}))

(defn- save-with-caused-by!
  "This is an auxillary function called primarily by put!. Should not be used in
  general APIs.

  It saves the passed exception's caused-by-id instead of it's caused-by train."
  [conn exception]
  (d/transact conn {:tx-data (form-for-exc exception (d/db conn) true)}))

(defn- rename-keys
  "Takes a map and a map of renames. Each instance of a key in the map matched by a key
  in the rename map will be replaced with the corresponding value in the rename map.
  For example: (rename-keys {:old 1, :foo 2} {:old :new}) returns {:new 1, :foo 2}."
  [m key-map]
  (reduce (fn [coll k]
            (if (contains? coll k)
              (-> coll
                  (assoc (k key-map) (k coll))
                  (dissoc k))
              coll))
          m (keys key-map)))

(defn fetch
  "Get the exception with the specified id from the database."
  [db id]
  (when id
    (let  [result (rename-keys
                   (d/pull db
                           '[:exception/class
                             :exception/message
                             {:exception/clojure-version [:clojure/version]}
                             :exception/caused-by
                             :exception/timestamp
                             :exception/phase
                             {:exception/message-explanation [:message/explanation]}
                             {:exception/stack-trace-element
                              [(:stack-trace-element/class :as :class)
                               (:stack-trace-element/method :as :method)
                               (:stack-trace-element/file :as :file)
                               (:stack-trace-element/line :as :line)]}] id)
                   {:exception/class :class
                    :exception/message :message
                    :exception/clojure-version :clojure-version
                    :exception/timestamp :timestamp
                    :exception/phase :phase
                    :exception/caused-by :caused-by-id
                    :exception/message-explanation :message-explanation
                    :exception/stack-trace-element :stack-trace})]
      (when result
        (assoc result
               :clojure-version (get-in result [:clojure-version :clojure/version])
               :caused-by-id (get-in result [:caused-by-id :db/id])
               :message-explanation (get-in result [:message-explanation :message/explanation])
               :id id)))))

(defn find-caused
  "Returns the exception ids of all the exceptions the passed exception causes."
  [db id]
  (set (map first (d/q
                   '[:find ?e :in $ ?caused :where [?e :exception/caused-by ?caused]]
                   db
                   id))))

(defn list
  "Lists all the exceptions in db from start to end (defaults to 0 to 100).

  If min-occurrences is passed in options, then only return exceptions that
  have at least that number of occurrences.

  These exceptions are not sorted in any way."
  ([db {:keys [start end min-occurences]
        :or {start 0 end 100 min-occurences 0}}]
   (map
    (partial fetch db)
    (->> (map
          first
          (d/q '[:find ?e :in $ ?o :where [?e :exception/occurences ?c]
                                          [(>= ?c ?o)]]
               db min-occurences))
         sort
         (drop start)
         (take (- end start)))))
  ([db]
   (list db {})))

(defn exception-count
  "Returns the total count of exceptions, optionally only including exceptions
  with a number of occurences more than or equal min-occurrences"
  ([db]
   (exception-count db 0))
  ([db min-occurences]
   (count (d/q '[:find ?e :in $ ?o :where [?e :exception/occurences ?c]
                                          [(>= ?c ?o)]]
              db min-occurences))))


(defn ste-subquery
  "Given indexes and a stack trace element (ste), return a Datomic 'subquery'
  with args, variables, and where clauses according to the info in the stack
  trace and variable names informed by the indexes."
  [exception-index ste-index ste]

  (letfn [(make-sym
            ([base] (make-sym ste-index base))
            ([ste-index base]
             (symbol (format "?ex%d-ste%d-%s"
                             exception-index ste-index base))))]

    (let [symbols {:class (make-sym "class-in")
                   :method (make-sym "method-in")
                   :file (make-sym "file-in")
                   :line (make-sym "line-in")
                   :eid (make-sym "eid")
                   :index (make-sym "index")
                   :exception (symbol (format "?ex%d-eid" exception-index))}

          make-rule (fn [attr]
                      [(:eid symbols)
                       (keyword "stack-trace-element" (name attr))
                       (attr symbols)])

          attrs (if (and (contains? ste :file) (:file ste))
                  [:class :method :file :line]
                  [:class :method])

          rules (mapv make-rule (conj attrs :index))
          rules (if (pos? ste-index)
                  (let [predicate [(clojure.core/list '<
                                         (make-sym (dec ste-index) "index")
                                         (make-sym ste-index "index"))]]
                    (conj rules predicate))
                  rules)
          rules (conj rules [(:exception symbols)
                             :exception/stack-trace-element
                             (:eid symbols)])]
      {:args (mapv ste attrs)
       :query {:in (mapv symbols attrs)
               :where rules}})))

(defn merge-subqueries
  [subqueries]
  (let [mapcatv (comp vec mapcat)]
    {:args (mapcatv :args subqueries)
     :query {:in (mapcatv #(-> % :query :in) subqueries)
             :where (mapcatv #(-> % :query :where) subqueries)}}))

(defn stack-trace-subquery
  [exception-index stack-trace]
  (merge-subqueries
    (let [filtered (filter-stack-trace stack-trace)]
      (vec (for [i (range (count filtered))]
             (ste-subquery exception-index i (nth filtered i)))))))

(defn- is-compile-phase?
  [exception]
  (some #{(:phase exception)} #{:compile-syntax-check :compilation}))

(defn base-exception-subquery
  [exception exception-index version-inheritance]
  (letfn [(make-sym [base]
            (symbol (format "?ex%d-%s" exception-index base)))]
    (let [clj-version-sym (make-sym "clojure-version-in")
          clj-version-eid-sym (make-sym "clojure-version-eid")
          class-sym (make-sym "class-in")
          eid-sym (make-sym "eid")
          phase-sym (make-sym "phase")
          message-sym (make-sym "message")
          message-match-sym (make-sym "message-match")
          comparison-sym (make-sym "jaccarard-index")
          clj-version-inheritance-sym (make-sym "clj-version-inheritance")
          clj-version-inheritance-eid-sym (make-sym "clj-version-eid-inheritance")
          phase (:phase exception)
          version-query (if version-inheritance
                          [[eid-sym :exception/clojure-version clj-version-inheritance-eid-sym]
                           [clj-version-inheritance-eid-sym :clojure/version clj-version-inheritance-sym]
                           [(clojure.core/list 'version-clj.core/older-or-equal? clj-version-inheritance-sym
                                                              clj-version-sym)]]
                          [[eid-sym :exception/clojure-version clj-version-eid-sym]
                           [clj-version-eid-sym :clojure/version clj-version-sym]])
          message-match-query [[eid-sym :exception/message message-match-sym]
                               [(clojure.core/list 'edu.unc.applab.clem.db.query-functions/jaccard
                                                   message-match-sym
                                                   message-sym) comparison-sym]
                               [(clojure.core/list '> comparison-sym min-jaccard-index)]]
          main-query [[eid-sym :exception/phase phase-sym]
                      [eid-sym :exception/class class-sym]]]
      (if (is-compile-phase? exception)
        {:args (mapv exception [:clojure-version :class :phase :message])
         :query {:in [clj-version-sym class-sym phase-sym message-sym]
                 :where (concat main-query version-query message-match-query)}}
        ;; Figure out how to do searching on multiple exceptions
        {:args (mapv exception [:clojure-version :class :phase])
         :query {:in [clj-version-sym class-sym phase-sym]
                 :where (concat main-query version-query)}}))))

(defn exception-query
  ([db exception]
   (exception-query db exception {} 0 exception-query))
  ([db exception options] (exception-query db exception options 0 exception-query))
  ;; The reason for the 'self' argument here is for dependency injection, to
  ;; simplify how we test the recursive calls.
  ([db exception options self] (self db exception options 0 self))
  ([db
    exception
    {:keys [version-inheritance] :as options}
    exception-index self]
   (let [result
         (if (is-compile-phase? exception)
           (base-exception-subquery exception exception-index version-inheritance)
           (merge-subqueries [(stack-trace-subquery exception-index
                                                    (:stack-trace exception))
                              (base-exception-subquery exception exception-index version-inheritance)]))
         result (if-let [cb-exception false #_(:caused-by exception)]
                  (merge-subqueries
                    [result (self db cb-exception options (inc exception-index) self)])
                  result)]
     (if (zero? exception-index)
       (-> result
           (update-in [:query :in] #(into ['$] %))
           (update :args #(into [db] %))
           (assoc-in [:query :find] '[?ex0-eid]))
       result))))

(defn find
  [db exception]
  (ffirst (d/q (exception-query db exception))))

(defn- find-versions-messages
  [db exception-ids]
  (map #(d/q '[:find ?clojure-version ?message-explanation
               :in $ ?e
               :where [?e :exception/clojure-version ?cv]
               [?cv :clojure/version ?clojure-version]
               [?e :exception/message-explanation ?me]
               [?me :message/explanation ?message-explanation]]
             db
             %)
       exception-ids))

(defn inherited-explanation
  [db exception]
  (->> (d/q (exception-query db exception {:version-inheritance true}))
       (map first)
       (find-versions-messages db)
       (map first)
       (filter #(not (nil? (second %))))
       (sort #(version-clj.core/version-compare
               (first %1)
               (first %2)))
       reverse
       first
       second))

(defn put!
  "Takes an exception and commits it to the database if it is not already present in
  the database.

  Returns a found-exception matching the one in the database (including id) with the
  stack-trace and message of the passed exception. It will contain metadata with the key
  :new which will be true or false, depending on if the exception was added to the
  database or if it already existed."
  [conn exception]
  (-> (if-let [db-exc (fetch (d/db conn) (find (d/db conn) exception))]
        (with-meta (as-> db-exc exc
                     (if-not (:message-explanation exc)
                       (assoc exc :message-explanation (inherited-explanation (d/db conn) exc))
                       exc)
                     (assoc exc :stack-trace (:stack-trace exception))
                     (assoc exc :message (:message exception)))
          {:new false})
        (with-meta (let [caused-by-id (if (:caused-by exception)
                                        (:id (put! conn (:caused-by exception))))]
                     (as-> exception exc
                         (assoc exc :id (find (:db-after (save-with-caused-by!
                                                      conn
                                                      (assoc exception :caused-by nil
                                                             :caused-by-id caused-by-id)))
                                          exception))
                         (if-not (:message-explanation exc)
                           (assoc exc :message-explanation (inherited-explanation (d/db conn) exc))
                           exc)
                         (assoc exc :caused-by-id caused-by-id)
                         (dissoc exc :caused-by)))
          {:new true}))))

(defn increment-occurrences!
  "Attempts to add one to the occurrence count for the passed exception id"
  [conn eid]
  (let [old-count (ffirst (d/q '[:find ?c :in $ ?e :where [?e :exception/occurences ?c]] (d/db conn) eid))]
    (try
      (if old-count
        (d/transact conn {:tx-data [[:db/cas eid :exception/occurences old-count (inc old-count)]]})
        (d/transact conn {:tx-data [[:db/cas eid :exception/occurences nil 1]]}))

      (catch java.lang.IllegalStateException e
        (increment-occurrences! conn eid)))))

(defn fetch-clojure-versions
  "Returns all clojure versions used by exceptions in the database."
  [db]
  (map first
       (d/q
        '[:find ?v
          :where [_ :clojure/version ?v]]
        db)))

(defn fetch-occurence-listing
  "Given an sequence of exception ids, it returns a map with the exception ids as keys
  and the occurrence counts as values"
  [db eids]
  (reduce (fn [result id]
            (assoc result id (fetch-occurences db id)))
          {}
          eids))

(defn tag-string
  [val]
  (with-meta val {:tag 'java.lang.String}))

(defn search
  "Given a list of strings and a db, searches the database for all exceptions that
  contain said string in explanations, exception messages, a stracktrace element's
  filename, or a stacktrace element's file name. Returns the exception ids"
  [db strings]
  ;; This binding prevents reflection warnings caused by datomic when using the .contains below
  (binding [*warn-on-reflection* false]
    (->>
     {:query {:find '[?e]
              :where (into []
                           (map
                            (fn [value]
                              (clojure.core/list
                               'or-join ['?e]
                               (clojure.core/list
                                'and
                                ['?e :exception/message '?text]
                                [(clojure.core/list '.contains ^java.lang.String '?text value)])
                               (clojure.core/list
                                'and
                                ['?e :exception/message-explanation '?explanation]
                                '[?explanation :message/explanation ?expl-text]
                                [(clojure.core/list '.contains ^java.lang.String '?expl-text value)])
                               (clojure.core/list
                                'or
                                (clojure.core/list
                                 'and
                                 '[?e :exception/stack-trace-element ?ste]
                                 '[?ste :stack-trace-element/file ?file]
                                 [(clojure.core/list '.contains ^java.lang.String '?file value)]))
                               (clojure.core/list
                                'or
                                (clojure.core/list
                                 'and
                                 '[?e :exception/stack-trace-element ?ste]
                                 '[?ste :stack-trace-element/method ?method]
                                 [(clojure.core/list '.contains ^java.lang.String '?method value)]))))
                            strings))}
      :args [db]}
     (d/q)
     (map first))))
