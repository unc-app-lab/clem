(ns edu.unc.applab.clem.db.explanation
  (:require [datomic.client.api :as d]
            [edu.unc.applab.clem.spec :as spec]))

(defn save! [conn explanation user]
  (let [{:keys [exception-id explanation-string]} explanation
        exception-id-query '[:find ?e
                             :in $ ?e
                             :where [?e :exception/class _]]
        results (d/q exception-id-query (d/db conn) exception-id)]
    (when (not-empty results)
      (let [tempid "new-message-tempid"]
        (d/transact conn
                    {:tx-data
                     [[:db/add exception-id :exception/message-explanation tempid]
                      [:db/add tempid :message/explanation explanation-string]
                      [:db/add tempid :message/user user]]})))))

(defn history
  "Returns a seq of all explanations that the exception with the passed id has had,
  sorted in the order they were created, with the newest first. Duplicates are preserved."
  [db eid]
  (->>
   (d/q '[:find ?expl ?inst
          :in $ ?e
          :where [?e :exception/message-explanation ?explid]
          [?explid :message/explanation ?expl ?explt]
          [?explt :db/txInstant ?inst]]
        (d/history db)
        eid)
   (sort-by #(.getTime (second %)))
   reverse
   (map first)))

(comment
  ;;all of this is for testing purposes, please
  (d/q '[:find ?eid :in $ ?eid :where [?eid]] (d/db conn) 76567687)
  ;;testing
  (require '[compute.datomic-client-memdb.core :as cc]
           '[datomic.client.api :as d]
           '[edu.unc.applab.clem.db.schema :as s])
  (def client (cc/client {}))
  (d/create-database client {:db-name "test"})
  (def conn (d/connect client {:db-name "test"}))
  (s/install-schema! conn)

  (def fake-stack-trace [{:db/id "stacktrace"
                          :stack-trace-element/index 5445345
                          :stack-trace-element/class "classy-class"
                          :stack-trace-element/method "fake-method"
                          :stack-trace-element/file "file thing"
                          :stack-trace-element/line 43565}])

  (def stack '[:find ?index
               :where [?e :stack-trace-element/method ?index]
               [?e :stack-trace-element/class "classy-class"]])
  ;;(def caused-by nil)

  (def test-exception [{:db/id "adse"
                        :clojure/version "1.9.0"}
                       {:db/id "adsa"
                        :stack-trace-element/index 5445345
                        :stack-trace-element/class "classy-class"
                        :stack-trace-element/method "fake-method"
                        :stack-trace-element/file "file thing"
                        :stack-trace-element/line 43565}
                       {:db/id "issues here"
                        :message/explanation "fhgwesvkdbe"}
                       {:exception/clojure-version "adse"
                        :exception/class "Runtime"
                        :exception/message "string"
                        :exception/stack-trace-element ["adsa"]
                        :exception/message-explanation "issues here"}])

  (def test-exception2 [{:db/id "hi"
                         :clojure/version "1.9.0"}
                        {:db/id "um"
                         :stack-trace-element/index 5445345
                         :stack-trace-element/class "classy"
                         :stack-trace-element/method "fake-method"
                         :stack-trace-element/file "file thing"
                         :stack-trace-element/line 43565}
                        {:db/id "issues here"
                         :message/explanation "fhgwesvkdbe"}
                        {:exception/clojure-version "hi"
                         :exception/class "Runtime"
                         :exception/message "more string"
                         :exception/stack-trace-element ["um"]
                         :exception/message-explanation "issues here"}])



  (d/transact conn {:tx-data test-exception})

  (def exception-id-query [:find '?e ;; :in $ ?exception-id  ;; move quote outside []
                           :where ['?e :exception/message ;; replace with ?exception-id
                                   ]])

  )
