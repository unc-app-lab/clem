(ns edu.unc.applab.clem.db.query-functions
  (:require [clojure.set]
            [clojure.string]))

(defn jaccard
  "Compute the Jaccard distance between [str1] and [str2]. Strings will be split on
  whitespace"
  [str1 str2]
  (let [set1 (set (clojure.string/split str1 #"\s"))
        set2 (set (clojure.string/split str2 #"\s"))]
    (/ (count (clojure.set/intersection set1 set2))
       (count (clojure.set/union set1 set2)))))
