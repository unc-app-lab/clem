(ns edu.unc.applab.clem.db.schema
  (:require [datomic.client.api :as d]))

(def stack-trace-element-schema
  [{:db/ident :stack-trace-element/index
    :db/valueType :db.type/long
    :db/cardinality :db.cardinality/one
    :db/doc "The index of a stack trace element within its stack trace vector"}
   {:db/ident :stack-trace-element/class
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The class of a stack trace element"}
   {:db/ident :stack-trace-element/method
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The method of a stack trace element"}
   {:db/ident :stack-trace-element/file
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The file of a stack trace element"}
   {:db/ident :stack-trace-element/line
    :db/valueType :db.type/long
    :db/cardinality :db.cardinality/one
    :db/doc "The line number of a stack trace element"}])

(def clojure-version-schema
  [{:db/ident :clojure/version
    :db/valueType :db.type/string
    :db/unique :db.unique/identity
    :db/cardinality :db.cardinality/one
    :db/doc "A version of Clojure"}])

(def exception-schema
  [{:db/ident :exception/clojure-version
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one
    :db/doc "The version of Clojure producing an exception"}
   {:db/ident :exception/class
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The class of an exception"}
   {:db/ident :exception/message
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The message of an exception"}
   {:db/ident :exception/stack-trace-element
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/many
    :db/doc "The stack trace elements of an exception"}
   {:db/ident :exception/caused-by
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one
    :db/doc "The exception causing an exception (or nil)"}
   {:db/ident :exception/message-explanation
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one
    :db/doc "The friendly message explanation for an exception (or nil)"}
   {:db/ident :exception/timestamp
    :db/valueType :db.type/long
    :db/cardinality :db.cardinality/one
    :db/doc "The time the exception was first captured."}
   {:db/ident :exception/occurences
    :db/valueType :db.type/long
    :db/cardinality :db.cardinality/one
    :db/doc "How many times the exception has been registered by Clem users"}
   {:db/ident :exception/phase
    :db/valueType :db.type/keyword
    :db/cardinality :db.cardinality/one
    :db/doc "The exception's phase, as determined by ex-triage. Should be one of
             [:read-source :compile-syntax-check :compilation :macro-syntax-check
             :macroexpansion :execution :read-eval-result :print-eval-result]"}])

(def message-schema
  [{:db/ident :message/explanation
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The friendly message explanation string"}
   {:db/ident :message/user
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "The user who made the most recent change to this message (the current change)"}])

;; TODO:
;; - add users and make messages owned by users.
;; - consider tracking message revisions explicitly in the schema rather than
;;   implicitly in the way Datomic tracks how values change in time.

(def schema
  (vec
    (concat stack-trace-element-schema
            clojure-version-schema
            exception-schema
            message-schema)))

(defn install-schema! [conn]
  (d/transact conn {:tx-data schema}))

(comment
  ;; getting a database connection
  (require '[edu.unc.applab.clem.system :as system])
  (system/start-system system/dev-system-config)
  (def system *1)
  (require '[datomic.client.api :as d]
           '[edu.unc.applab.clem.db.exception]
           '[clojure.spec.alpha :as s]
           '[clojure.spec.gen.alpha :as gen])
  (def conn (:datomic/conn system))
  (def exc (gen/generate (s/gen :edu.unc.applab.clem.spec/exception)))
  (edu.unc.applab.clem.db.exception/put! conn exc)
  )
