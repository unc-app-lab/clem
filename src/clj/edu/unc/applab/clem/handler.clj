(ns edu.unc.applab.clem.handler
  (:require [buddy.auth.backends.session :refer [session-backend]]
            [buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
            [buddy.auth :refer [authenticated? throw-unauthorized]]
            [clj-http.client :as client]
            [clojure.edn :as edn]
            [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]
            [clojure.string :as string]
            [datomic.client.api :as d]
            [edu.unc.applab.clem.auth :as auth]
            [edu.unc.applab.clem.middleware :refer [wrap-content-type wrap-username] :as middleware]
            [datomic.ion.lambda.api-gateway :as apigw]
            [edu.unc.applab.clem.db.exception :as exc]
            [edu.unc.applab.clem.db.explanation :as expl]
            [edu.unc.applab.clem.spec :as clem-spec]
            [ring.adapter.jetty9 :as jetty]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.session :refer [wrap-session]]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.middleware.session.cookie :refer [cookie-store]]
            [ring.middleware.session.memory :refer [memory-store]]
            [ring.util.request :as req]
            [ring.util.response :as resp :refer [resource-response]])
  (:import java.util.Base64))

(def not-found-page {:status 404, :body "Clem could not find this page."})

(defn unauthorized-handler
  [request metadata]
  (if (authenticated? request)
    {:status 403
     :body "Not authorized to perform this action"}
    {:status 401
     :body "Not logged in"}))

(def backend (session-backend {:unauthorized-handler unauthorized-handler}))

(defn decode-base64 [to-decode]
  (String. (.decode (Base64/getDecoder) to-decode)))

(defn- convert-ids
  [exc convert-fn]
  (cond-> exc
    (:id exc) (update :id convert-fn)
    (:caused-by-id exc) (update :caused-by-id convert-fn)
    (:caused-by exc) (update :caused-by convert-ids convert-fn)))

(defn convert-id-to-string
  "Converts all keys of ':id' and ':caused-by-id' to strings."
  [exc]
  (convert-ids exc str))

(defn convert-string-to-id
  "Converts all keys of ':id' and ':caused-by-id' to longs. Assumes they are valid longs."
  [exc]
  (convert-ids exc #(Long/parseLong %)))

(defn- get-error-id
  "Gets the error id for the passed url. Returns nil if the error id not found."
  [uri]
  (Long/parseLong (second (re-matches #"\/api\/error\/([0-9]*)(\/.*|$)" uri))))

(defn numeric? [s]
  ;;Borrowed from https://rosettacode.org/wiki/Determine_if_a_string_is_numeric#Clojure
  (if-let [s (seq s)]
    (empty? (drop-while #(Character/isDigit %) s))))

(defn matches-whitelist
  [whitelist url]
  (if url
    (some #(re-find (re-pattern (str "^" %)) url) whitelist)
    false))

(defn insert-explanation [request]
  (if (authenticated? request)
    (let [params (:params request)
          eid (when (numeric? (get params "exception-id"))
                (Long/parseLong (get params "exception-id")))
          expl {:exception-id eid,
                :explanation-string (get params "explanation-string")}]
      (if (s/valid? ::clem-spec/explanation expl)
        (if (expl/save! (:datomic/conn request) expl (get-in request [:session :identity :sub]))
          {:status 200
           :body "Explanation successfully saved."}
          {:status 404
           :body "Exception not found."})
        {:status 400
         :body "Exception id or explanation-string badly formatted or not found."}))
    (throw-unauthorized)))

(defn put-exception [request]
  (if (= (:content-type request) "application/edn")
    (let [exception (try
                      (-> request
                          req/body-string
                          edn/read-string
                          convert-string-to-id)
                      (catch RuntimeException ex
                        :bad-data))] ; :bad-data is guaranteed to fail the spec check below
      (if (s/valid? :edu.unc.applab.clem.spec/exception exception)
        (let [result (exc/put! (:datomic/conn request) exception)]
          (exc/increment-occurrences! (:datomic/conn request) (:id result))
          {:status (if (:new (meta result))
                     201
                     200)
           :content-type "application/edn"
           :body (pr-str (convert-id-to-string result))})
        {:status 400
         :body (if-not (= :bad-data exception)
                 (s/explain-str :edu.unc.applab.clem.spec/exception exception)
                 "EDN failed to parse. Probably it is improperly formatted.")}))
    {:status 415
     :body "Content type needs to be application/edn for this request"}))

(defn get-error [request]
  (let [id (get-error-id (:uri request))
        db (d/db (:datomic/conn request))
        exc (convert-id-to-string (exc/fetch db id))
        caused-by (map str (exc/find-caused db id))]
    (if exc
      {:status 200
       :content-type "application/edn"
       :body (pr-str [exc caused-by])}
      {:status 404
       :body "No such exception"})))

(defn list-errors [db request]
  (try
    {:status 200
     :content-type "application/edn"
     :body (pr-str (map
                    convert-id-to-string
                    (exc/list db (reduce-kv (fn [m k v]
                                              (assoc m (keyword k) (Integer/parseInt v)))
                                            {}
                                            (:params request)))))}
    (catch NumberFormatException e
      {:status 400
       :body "Invalid parameters. Expected number and received string."})))

(defn- convert-keys-to-strings
  [m]
  (into {} (map #(update % 0 str) m)))

(defn get-occurrences [request]
  (if (= (:content-type request "application/edn"))
    (try
      (let [ids (map #(Long/parseLong %) (edn/read-string (req/body-string request)))]
        {:status 200
         :content-type "application/edn"
         :body (pr-str (convert-keys-to-strings (exc/fetch-occurence-listing (d/db (:datomic/conn request)) ids)))})
      (catch RuntimeException ex
        {:status 400
         :body (.getMessage ex)}))
    {:status 415
     :body "Content type needs to be application/edn for this request"}))

(defn get-versions [db]
  {:status 200
   :content-type "application/edn"
   :body (pr-str (exc/fetch-clojure-versions db))})

(defn- get-protocol-string [scheme]
  (str (string/join (rest (str scheme))) "://"))

(defn sign-in
  "This endpoint is called when a user goes to sign in. It redirects them to the Amazon
  Cognito signin ui."
  [action
   {{:keys [sign-in client-id]} :cognito
    :keys [server-name server-port scheme params session prefix]}]
  {:status 302
   :session (assoc session :redirect (get params "redirect"))
   :headers {"Location"
             (format
              "%s/%s?response_type=code&client_id=%s&redirect_uri=%s%s:%s%s/api/signed-in"
              sign-in
              action
              client-id
              (get-protocol-string scheme)
              server-name
              server-port
              (or prefix ""))}})

(defn signed-in
  "The Amazon Cognito UI redirects here once the user has signed in. This processes the
  tokens from Cognito in order to get the user's actual information in a secure way."
  [{:keys [params scheme server-name server-port session prefix]
    {:keys [sign-in client-id region user-pool-id whitelist]} :cognito}]
  (let [{tokens :body} (client/post (str sign-in "/oauth2/token")
                              {:form-params {:grant_type "authorization_code"
                                             :code (get params "code")
                                             :client_id client-id
                                             :redirect_uri (format "%s%s:%s%s/api/signed-in" (get-protocol-string scheme) server-name server-port (or prefix ""))}
                               :content-type "application/x-www-form-urlencoded"
                               :as :json
                               :throw-exceptions? false})
        ident (auth/decode-jwt! (:id_token tokens) region user-pool-id)
        redirect-target (:redirect session)]
    (update
     (resp/redirect (if (matches-whitelist whitelist redirect-target)
                      redirect-target
                      (str (or prefix "") "/")))
     :session
     #(assoc % :identity ident))))

(defn sign-out
  "User's call this endpoint in order to sign out of Cognito."
  [{{:keys [sign-in client-id]} :cognito
    :keys [server-name server-port scheme session params prefix]}]
  {:status 302
   :session (assoc session :redirect (get params "redirect"))
   :headers {"Location" (format
                         "%s/logout?client_id=%s&logout_uri=%s%s:%s%s/api/signed-out"
                         sign-in
                         client-id
                         (get-protocol-string scheme)
                         server-name
                         server-port
                         (or prefix ""))}})

(defn signed-out
  "Amazon Cognito redirects here after a user has signed-out"
  [{:keys [cognito session prefix]}]
  (let [redirect-target (:redirect session)
        whitelist (:whitelist cognito)]
    (assoc-in
     (resp/redirect (if (matches-whitelist whitelist redirect-target)
                      redirect-target
                      (str (or prefix "") "/")))
     [:session :identity]
     nil)))

(defn get-identity [{:keys [session]}]
  (let [identity (:identity session)]
    {:status 200
     :content-type "application/edn"
     :body (pr-str (if identity
                     {:username (:cognito:username (:identity session))}
                     nil))}))

(defn get-exception-count [{conn :datomic/conn
                            {:keys [min-occurences]} :params}]
  {:status 200
   :content-type "application/edn"
   :body (pr-str (exc/exception-count (d/db conn) (or min-occurences 0)))})

(defn routes [request]
  ;; So here, for example, you can say (:datomic/conn request) to get the
  ;; Datomic connection.
  (let [data [(:request-method request) (:uri request)]]
    (cond
      (= data [:get "/"])               (assoc (resource-response "index.html" {:root "public"}) :content-type "text/html")
      (= data [:get "/api/errors"])         (list-errors (d/db (:datomic/conn request)) request)
      (= data [:post "/api/occurrences"])     (get-occurrences request)
      (re-matches #"\/api\/error/[0-9]*\/?$" (:uri request)) (get-error request)
      (= data [:post "/api/explanation"]) (insert-explanation request)
      (= data [:put "/api/error"]) (put-exception request)
      (= data [:get "/api/count"]) (get-exception-count request)
      (= data [:get "/api/versions"]) (get-versions (d/db (:datomic/conn request)))
      (= data [:get "/api/signin"]) (sign-in "login" request)
      (= data [:get "/api/signed-in"]) (signed-in request)
      (= data [:get "/api/sign-up"]) (sign-in "signup" request)
      (= data [:get "/api/identity"]) (get-identity request)
      (= data [:get "/api/signout"]) (sign-out request)
      (= data [:get "/api/signed-out"]) (signed-out request)
      :default                not-found-page)))



(def handler (-> routes
                 (wrap-resource "public")
                 wrap-params
                 wrap-content-type
                 (wrap-authentication backend)
                 (wrap-authorization backend)
                 ))
(def dev-handler (wrap-reload handler))

;;Different handler used for ions; See edu.unc.applab.clem.ion/ion-handler

(defn start-server [config]
  (let [{:keys [port handler conn cognito cookie-secret]} config
        wrapped-handler (-> handler
                            (wrap-session {:store (if cookie-secret
                                                    (cookie-store {:key cookie-secret})
                                                    (memory-store))})
                            (wrap-cors :access-control-allow-origin (map re-pattern (:whitelist cognito))
                                       :access-control-allow-methods [:get :put :post])
                            (middleware/inject-config-in-request-map conn cognito))]
    (jetty/run-jetty wrapped-handler {:port port, :join? false})))

(defn stop-server [server]
  (.stop server))
