(ns edu.unc.applab.clem.ion
  (:require [clojure.edn :as edn]
            [datomic.client.api :as d]
            [datomic.ion.cast :as cast]
            [datomic.ion.lambda.api-gateway :as apigw]
            [edu.unc.applab.clem.config :as config]
            [edu.unc.applab.clem.handler :as handler]
            [edu.unc.applab.clem.system :as system]
            [edu.unc.applab.clem.middleware :as m]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.middleware.session :refer [wrap-session]]
            [ring.middleware.session.cookie :refer [cookie-store]]
            [clojure.set :as cset]
            [clojure.java.io :as io]))

;; Some code modified from https://github.com/Datomic/ion-starter/blob/master/src/datomic/ion/starter.clj

(def datomic-start "/datomic")

(def get-client
  "Return a shared client. Set datomic/ion/clem-config.edn resource
before calling this function."
  (memoize #(if-let [r (io/resource "datomic/clem-config.edn")]
              (d/client (edn/read-string (slurp r)))
              (throw (RuntimeException. "You need to add a resource datomic/clem-config.edn with your connection config")))))

(defn get-conn []
  (d/connect (get-client) {:db-name (get-in system/dev-system-config [:datomic/conn :db-name])}))


(defn fix-cookies [handler]
  (fn [request]
    (let [response (handler request)]
      (if (contains? (:headers response) "Set-Cookie")
        (let [cookie (get-in response [:headers "Set-Cookie"])]
          (if (coll? cookie)
            (do
              (cast/event {:msg "Found invalid seq cookie, simply keeping the first item."
                           :original-header cookie})
              (assoc-in response [:headers "Set-Cookie"] (first cookie)))
            response))
        response))))

;; This handler is used for ions, the start-server function in edu.unc.applab.clem.handler
;; sets up the handler for a local server
(defn ion-handler [request]
  (try
    (let [config-options (config/create-environment {}) ; We don't use cli-arguments with ions
          cognito-config (config/generate-cognito-config config-options)
          wrapped-handler (-> handler/handler
                              (wrap-session {:store (cookie-store {:key (:cookie-secret config-options)})})
                              (wrap-cors :access-control-allow-origin (map re-pattern (:whitelist cognito-config))
                                         :access-control-allow-credentials "true"
                                         :access-control-allow-methods [:get :put :post])
                              (m/inject-config-in-request-map
                               (get-conn)
                               cognito-config)
                              fix-cookies)
          response (wrapped-handler
                    (update request :uri #(if (.startsWith % datomic-start)
                                            (.substring % (count datomic-start))
                                            %)))]
      (if (instance? java.io.File (:body response))
        (update response :body slurp)
        response))
    (catch Throwable ex
      (cast/alert {:msg "Exception" :ex ex})
      (throw ex))))

(def ion-handler-lambda-proxy (apigw/ionize (fn [request]
                                              (ion-handler
                                               (as-> request r
                                                 (assoc r :scheme (keyword (get-in r [:headers "x-forwarded-proto"])))
                                                 (assoc r :server-port (Integer/parseInt (get-in r [:headers "x-forwarded-port"])))
                                                 (assoc r :prefix "/Default/datomic"))))))
