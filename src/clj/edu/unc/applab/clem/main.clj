(ns edu.unc.applab.clem.main
  (:require clojure.string
            [clojure.tools.cli :refer [parse-opts]]
            [edu.unc.applab.clem.handler :as handler :refer [dev-handler handler]]
            [edu.unc.applab.clem.system :as system]
            [edu.unc.applab.clem.config :as config]
            [ring.adapter.jetty9 :refer [run-jetty]])
  (:gen-class))


(defn -main
  "Starts the project from the commandline.

  Options are specified in edu.unc.applab.clem.config/cli-options

  This is not used for an ions/production setup. See the handler in edu.unc.applab.clem.ion"
  [& args]
  (let [parsed-args (parse-opts args config/cli-options)
        {:keys [port devel region system endpoint sign-in-url client-id
                auth-region user-pool-id redirect-whitelist cookie-secret] :as options} (config/create-environment parsed-args)
        base-cfg (-> system/dev-system-config
                     (assoc-in [:server/http :port] port)
                     (assoc-in [:server/http :cookie-secret] cookie-secret)
                     (assoc-in [:server/http :cognito] (config/generate-cognito-config options)))
        config (if devel
                 (assoc-in base-cfg [:datomic/client :local?] true)
                 (-> base-cfg
                     (assoc-in [:server/http :handler] handler/handler)
                     (assoc-in [:datomic/client :region] region)
                     (assoc-in [:datomic/client :system] system)
                     (assoc-in [:datomic/client :endpoint] endpoint)))]
    (if (:errors parsed-args)
      (println (clojure.string/join "\n" (:errors parsed-args)))
      (system/start-system config))))

(comment
  ;; creating a system from the REPL and using it a bit
  (def system (system/start-system system/dev-system-config))
  (require '[datomic.client.api :as d])
  (def conn (:datomic/conn system))
  (d/transact conn {:tx-data [{:clojure/version "1.9.0"}]})
  (d/q '[:find ?v :where [_ :clojure/version ?v]] (d/db conn)) ; => #{["1.9.0"]}
  (edu.unc.applab.clem.system/stop-system system)
  )

(comment
  ;;Setting up example databases when starting
  (require 'edu.unc.applab.clem.main)
  (edu.unc.applab.clem.main/-main "-d" "-i" "us-east-1_0XQW99k2s" "-a" "us-east-1" "-u" "https://clem.auth.us-east-1.amazoncognito.com" "-c" "475d73sj6tvbg0n1o8ut0mlgr5" "-w" "http://localhost,http://clem.applab.unc.edu" "-C" "16 character key")
  (def system *1)
  (def conn (:datomic/conn system))
  ;; This doall block will populate the database with example exceptions, probably only
  ;; want to run this when there's a new database
  (doall (->> (map (partial edu.unc.applab.clem.db.exception/put! conn)
              (clojure.spec.gen.alpha/sample
               (clojure.spec.alpha/gen (clojure.spec.alpha/keys :req-un [:edu.unc.applab.clem.spec/clojure-version :edu.unc.applab.clem.spec/class :edu.unc.applab.clem.spec/message :edu.unc.applab.clem.spec/message-explanation
                                                                         :edu.unc.applab.clem.spec/timestamp :edu.unc.applab.clem.spec/stack-trace :edu.unc.applab.clem.spec/caused-by
                                                                         :edu.unc.applab.clem.spec/phase]
                                               :opt-un [:edu.unc.applab.clem.spec/id])
                                       {:edu.unc.applab.clem.spec/caused-by #(clojure.spec.gen.alpha/return nil)})
               150))
              (map #(dotimes [c (inc (rand-int 6))]
                      (edu.unc.applab.clem.db.exception/increment-occurrences! conn (:id %))))))
  )

(comment
  "Datomic ion example params map"
  {:sign-in-url "https://clem.auth.us-east-1.amazoncognito.com" :client-id "475d73sj6tvbg0n1o8ut0mlgr5" :auth-region "us-east-1" :user-pool-id "us-east-1_0XQW99k2s" :cookie-secret "JDkleio38DslJK^4"})
