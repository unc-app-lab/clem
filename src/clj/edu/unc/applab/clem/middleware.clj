(ns edu.unc.applab.clem.middleware
  (:require [ring.util.response :as r]
            [ring.util.request :as req]))


(defn set-content-type [resp]
  (if-let [content-type (:content-type resp)]
    (r/content-type resp content-type)
    resp))

(defn set-req-content-type [req]
  (if-let [content-type (req/content-type req)]
    (assoc req :content-type content-type)
    req))

(defn wrap-content-type [handler]
  (fn
    ([request]
     (set-content-type (handler (set-req-content-type request))))
    ([request respond raise]
     (handler (set-req-content-type request) #(respond (set-content-type %)) raise))))

(defn set-username [resp identity]
  (if identity
    (update resp :headers #(assoc % "Username" (:cognito:username identity)))))

(defn wrap-username [handler]
  (fn
    ([request]
     (set-username (get-in request [:session :identity]) (handler request)))))

(defn wrap-exception [handler]
  (fn [request]
    (try
      (handler request)
      (catch Exception ex
        (println ex)
        (throw ex)))))

(defn inject-config-in-request-map
  "Ring middleware. Given a handler and a Datomic connection, add the connection
  to a request map under :datomic/conn before calling the handler, so that the
  handler can access the Datomic database given only the request map."
  [handler conn cognito]
  (fn [request]
    (handler (assoc request :datomic/conn conn
                    :cognito cognito))))
