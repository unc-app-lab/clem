(ns edu.unc.applab.clem.system
  (:require [clojure.spec.alpha :as s]
            [datomic.client.api :as d]
            [edu.unc.applab.clem.handler :as handler]
            [edu.unc.applab.clem.db.schema :as schema]
            [integrant.core :as ig]))

;;; Specify required arguments to system components and their types with
;;; `ig/pre-init-spec`:

(s/def ::endpoint string?)
(s/def ::system string?)
(s/def ::region string?)
(s/def ::remote-client (s/keys :req-un [::endpoint ::system ::region]))
(s/def ::local? true?)
(s/def ::in-memory-client (s/keys :req-un [::local?]))
(defmethod ig/pre-init-spec :datomic/client [_]
  (s/or :remote-client ::remote-client
        :in-memory-client ::in-memory-client))

(s/def ::db-name string?)
(s/def ::client some?)
(defmethod ig/pre-init-spec :datomic/conn [_]
  (s/keys :req-un [::db-name ::client]))

;;; Provide default arguments for system components with `ig/prep-key` (which
;;; requires calling `ig/prep` before `ig/init`):

(defmethod ig/prep-key :datomic/client [_ config]
  (merge {:server-type :cloud}
         config))

(defmethod ig/prep-key :server/http [_ config]
  (merge {:port 3000, :handler handler/handler}
         config))

;;; Start system components using `ig/init-key`:

(defmethod ig/init-key :datomic/client [_ config]
  (println (if (:local? config)
             (assoc config :server-type :dev-local
                    :system "dev")
             config))
  (d/client (if (:local? config)
              (assoc config :server-type :dev-local
                            :system "dev")
              config)))

(defmethod ig/init-key :datomic/conn [_ config]
  (let [{:keys [client db-name]} config]
    (d/create-database client {:db-name db-name})
    (let [conn (d/connect client {:db-name db-name})]
      (schema/install-schema! conn)
      conn)))

(defmethod ig/init-key :server/http [_ config]
  (handler/start-server config))

;;; Halt system components, if needed, using `ig/halt-key!`:

(defmethod ig/halt-key! :server/http [_ server]
  (handler/stop-server server))

(def dev-system-config
  {:datomic/client {}
   :datomic/conn {:client (ig/ref :datomic/client)
                  :db-name "clem"}
   :server/http {:port 3000
                 :handler handler/dev-handler
                 :conn (ig/ref :datomic/conn)}})

(def prod-system-config (atom dev-system-config))

(defn start-system
  "Given a system config, start the system."
  [system-config]
  (let [port (-> system-config :server/http :port)
        system (-> system-config ig/prep ig/init)]
    (println "Sytem started.")
    (when port
      (println "HTTP server listening on port" port))
    system))

(defn stop-system
  "Stop a running system returned from `start-system`."
  [system]
  (ig/halt! system))
