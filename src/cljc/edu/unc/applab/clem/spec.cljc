(ns edu.unc.applab.clem.spec
  (:require [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]))

(defn long?
  "Returns true if x is a java.lang.Long or an integer in JavaScript (since all numbers
  are 64-bit in JS)"
  [x]
  #?(:clj (= (type x) java.lang.Long)
     :cljs (int? x)))

(s/def ::long? (s/with-gen long? gen/large-integer))

(s/def ::class
  (s/with-gen string?
    #(gen/fmap (fn [[string prefix]] (str prefix string))
               (gen/tuple (gen/string-alphanumeric)
                          (s/gen #{"" "clojure." "java." "javax."})))))
(s/def ::method string?)
(s/def ::file string?)
(s/def ::line pos-int?)
(s/def ::stack-trace-element-with-file
  (s/keys :req-un [::class ::method ::file ::line]))
(s/def ::stack-trace-element-without-file
  (s/keys :req-un [::class ::method]))
(s/def ::stack-trace-element
  (s/or :with-file ::stack-trace-element-with-file
        :without-file ::stack-trace-element-without-file))
(s/def ::stack-trace
  (s/coll-of ::stack-trace-element :kind vector? :min-count 1))

(comment
  (gen/generate (s/gen ::stack-trace))
  #_[{:class "clojure.8vN6nWJJHATZkQGZHf33422Y",
      :method "D61Mk6EYj17",
      :file "3tZFy7VN29xEy1A226Iwg3DKBnirC",
      :line 92979}
     {:class "D5W8l",
      :method "W0ixKKK96UC",
      :file "72RQEd249yvPqk5wV",
      :line nil}
     ,,,])

(s/def ::id #?(:clj (s/or :pos-long? (s/and ::long? #(> % 0))
                          :pos-int? pos-int?)
               :cljs string?))
(def version-regex #"[0-9]+\.[0-9]+(\.[0-9]+)?(-.+)?")
(s/def ::clojure-version
  (s/with-gen
    (s/and string? #(re-matches version-regex %))
    (fn []
      (gen/fmap
       (fn [[major minor incremental qualifier snapshot]]
         (str major
              "."
              minor
              (when incremental
                (str "." incremental))
              (when (and qualifier (pos-int? (count qualifier)))
                (str "-" qualifier))
              (when snapshot
                "-SNAPSHOT")))
       (gen/one-of
        [(gen/tuple (s/gen (s/int-in 1 25)) (s/gen (s/int-in 0 30)) (s/gen (s/int-in 0 30)))
         (gen/tuple (s/gen (s/int-in 1 25)) (s/gen (s/int-in 0 30)))
         (gen/tuple (s/gen (s/int-in 1 25)) (s/gen (s/int-in 0 30)) (s/gen (s/int-in 0 30)) (s/gen string?))
         (gen/tuple (s/gen (s/int-in 1 25)) (s/gen (s/int-in 0 30)) (s/gen (s/int-in 0 30))
                    (s/gen string?) (s/gen string?) (s/gen boolean?))])))))
(s/def ::message string?)
(s/def ::timestamp (s/with-gen (s/and ::long? pos?)
                     #(gen/large-integer* {:min 0})))
(s/def ::phase #{:read-source :compile-syntax-check :compilation :macro-syntax-check
                 :macroexpansion :execution :read-eval-result :print-eval-result})
(s/def ::exception
  (s/keys :req-un [::clojure-version ::class ::message ::stack-trace]
          :opt-un [::id]))
(s/def ::caused-by (s/nilable ::exception))
(s/def ::message-explanation (s/nilable string?))
(s/def ::exception
  (s/keys :req-un [::clojure-version ::class ::message ::message-explanation
                   ::timestamp ::stack-trace ::caused-by ::phase]
          :opt-un [::id]))
(s/def ::caused-by-id nat-int?)
(s/def ::found-exception
  (s/keys :req-un [::clojure-version ::class ::message ::message-explanation
                   ::timestamp ::stack-trace ::caused-by-id]
          :opt-un [::id]))

(comment
  (gen/generate (s/gen ::exception))
  ;; Note: some details were elided below.
  #_{:clojure-version "5qVx1UII"
     :class "jgdu6i6"
     :message "Znj2lO2"
     :message-explanation "iOo288U0cZJ9CKz5MIuR"
     :stack-trace [{:class "javax.RjzW4qcqmK5sBew13mv"
                    :method "04Y0Zy9x5H385rG21mAnj21L3"
                    :file "JIxGECu4hLLWH369Ayu"
                    :line 104034}
                   ,,,]
     :caused-by {:clojure-version "Sn7f345HHuP7i5uvqCKL07Ki" ;; Full map for caused-by
                 :class "Q595U"
                 :message "cHQ4t7s78c7Ux9q"
                 :stack-trace [,,,]
                 :caused-by nil
                 :id 630416}}
  ;; or
  #_{:clojure-version "F32XZjwx597",
     :class "cX2Kl5iGj3VSShIrrxJ2J",
     :message "y2n5m5GF7f2w2W302Ehl72",
     :message-explanation "XJ07j3DJOOwYhG0WhnsjU06Do433J4",
     :stack-trace
     [{:class "d270Z8uBttN8LPf",
       :method "LIkhi928m6IM51La8n6e",
       :file "PlvHd8a0rlYRrkzr4qUkT2S912X8",
       :line 2}
      ,,,],
     :caused-by 385086, ;; Entity ID for caused-by
     :id 2130}
  )

(s/def ::exception-id pos-int?)
(s/def ::explanation-string
  (s/with-gen string?
              #(gen/fmap (fn [[s1 s2 s3]] (str s1 s2 s3))
                         (gen/tuple (gen/string-alphanumeric) (s/gen #{"" "{{input}}"}) (gen/string-alphanumeric)))))
(s/def ::explanation
  (s/keys :req-un [::exception-id ::explanation-string]))

(comment
  (gen/generate (s/gen ::explanation))
  #_{:exception-id 6
     :explanation-string "sz{{input}}SNL2"}
  )
