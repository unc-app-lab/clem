(ns edu.unc.applab.clem.events
  (:require [re-frame.core :as rf]
            [day8.re-frame.http-fx]
            [ajax.core :as ajax]
            [reitit.frontend.easy :as rfe]
            [clojure.edn :as edn]))

(def min-occurences 1)
(def exc-per-page 100)

(def edn-response-format
  {:description "Read response as an EDN document"
   :read (fn [xhrIo]
           (let [result (edn/read-string (.getResponseText ^js/String xhrIo))]
             ;; Ions returns the edn value inside a string, so it needs to be read twice sometimes
             ;; Of course, if it's actually a string this throws an error, hence we catch
             ;; then exception and simply return the original result
             (if (string? result)
               (try
                 (do
                   (println "Reading second time")
                   (edn/read-string result))
                 (catch js/Object e
                   result))
               result)))
   :content-type "application/edn"})

(def edn-request-format
  {:content-type "application/edn"
   :write #(pr-str %)})

(defn generate-api-url
  [cofx uri]
  (str (get-in cofx [:db :api-url]) "/api/" uri))

(rf/reg-sub
 ::generate-api-url
 (fn [db [_ uri]]
   (str (:api-url db) "/api/" uri)))

(rf/reg-event-fx
 ::initial-load
 (fn [{:keys [db]} [_ config]]
   {:dispatch [::load-home-page]
    :db (assoc db :api-url (if (:api-url config)
                             (:api-url config)
                             ""))}))

(rf/reg-fx
 :change-page
 (fn [[href params]]
   (rfe/push-state href params)))

(rf/reg-event-fx
 ;; This event is specified by http-fx. It fires if a response returns an error code and
 ;; no error handling event is registered for that request.
 :http-no-on-failure
 (fn [_ [_ resp]]
   (js/console.log "Error making request")
   (println resp)))

(rf/reg-sub
 ::current-page
 (fn [db _]
   (:current-page db)))

(rf/reg-event-db
 ::change-current-page
 (fn [db [_ page-component]]
   (assoc db :current-page page-component)))

(rf/reg-sub
 ::errors
 (fn [db _]
   (:errors db)))

(rf/reg-event-fx
 ::get-versions
 (fn [cofx _]
   {:http-xhrio {:uri (generate-api-url cofx "versions")
                 :with-credentials true
                 :method :get
                 :timeout 10000
                 :response-format edn-response-format
                 :on-success [::versions-loaded]}}))

(rf/reg-event-db
 ::versions-loaded
 (fn [db [_ result]]
   (assoc db :versions result)))

(rf/reg-sub
 ::versions
 (fn [db _]
   (:versions db)))

(rf/reg-sub
 ::identity
 (fn [db _]
   (:identity db)))

(rf/reg-event-fx
 ::get-identity
 (fn [cofx _]
   {:http-xhrio {:uri (generate-api-url cofx "identity")
                 :method :get
                 :timeout 10000
                 :response-format edn-response-format
                 :with-credentials true
                 :on-success [::get-identity-success]}}))

(rf/reg-event-db
 ::get-identity-success
 (fn [db [_ result]]
   (assoc db :identity result)))

(rf/reg-event-fx
 ::load-home-page
 (fn [cofx [_ {:keys [start end]
               :or {start 0 end 100}}]]
   {:http-xhrio {:uri (generate-api-url cofx "errors")
                 :method :get
                 :params {:min-occurences min-occurences
                          :start start
                          :end end}
                 :timeout 10000
                 :with-credentials true
                 :response-format edn-response-format
                 :on-success [::homepage-loaded]}
    :dispatch-n (list [::get-identity] [::get-versions] [::get-count])
    :db (assoc (:db cofx) :errors "Loading...")}))

(rf/reg-event-fx
 ::homepage-loaded
 (fn [cofx [_ resp]]
   (if (> (count resp) 0)
     {:db (assoc (:db cofx) :errors resp)
      :http-xhrio {:uri (generate-api-url cofx "occurrences")
                   :with-credentials true
                   :method :post
                   :timeout 10000
                   :response-format edn-response-format
                   :format edn-request-format
                   :params (map :id resp)
                   :on-success [::save-occurrences]}})))

(rf/reg-event-fx
 ::get-count
 (fn [cofx _]
   {:http-xhrio {:uri (generate-api-url cofx "count")
                 :with-credentials true
                 :params {:min-occurences min-occurences}
                 :method :get
                 :timeout 10000
                 :format edn-request-format
                 :response-format edn-response-format
                 :on-success [::count-loaded]}}))

(rf/reg-event-db
 ::count-loaded
 (fn [db [_ resp]]
   (assoc db :exc-count resp)))

(rf/reg-sub
 ::page-count
 (fn [db]
   (.ceil js/Math (/ (:exc-count db) exc-per-page))))

(rf/reg-event-fx
 ::change-error-page
 (fn [cofx [_ page]]
   (let [start (* exc-per-page (dec page))]
     {:db (assoc (:db cofx) :page page)
      :dispatch [::load-home-page {:start start :end (+ start exc-per-page)}]})))

(rf/reg-sub
 ::page
 (fn [db _]
   (or (:page db) 1)))

(rf/reg-event-db
 ::save-occurrences
 (fn [db [_ resp]]
   (assoc db :occurrences resp)))

(rf/reg-sub
 ::occurrences
 (fn [db [_ id]]
   (get-in db [:occurrences id])))

(rf/reg-sub
 ::fetch-all-occurences
 (fn [db _]
   (:occurrences db)))

(rf/reg-sub
 ::error
 (fn [db [_]]
   (:error db)))

(rf/reg-event-fx
 ::load-error
 (fn [cofx [_ {:keys [id]}]]
   {:http-xhrio {:uri (generate-api-url cofx (str "error/" id))
                 :with-credentials true
                 :method :get
                 :timeout 10000
                 :response-format edn-response-format
                 :on-success [::error-loaded]}
    :db (assoc (:db cofx) :error "Loading...")}))

(rf/reg-event-db
 ::error-loaded
 (fn [db [_ resp]]
   (assoc db :error resp)))

(rf/reg-event-fx
 ::upsert-explanation
 (fn [cofx [_ expl eid]]
   {:http-xhrio {:uri (generate-api-url cofx "explanation")
                 :with-credentials true
                 :method :post
                 :timeout 10000
                 :params {:exception-id eid
                          :explanation-string expl}
                 :format (ajax/url-request-format)
                 :response-format (ajax/text-response-format)
                 :on-success [::upsert-success expl eid]}}))

(rf/reg-event-fx
 ::upsert-success
 (fn [cofx [_ expl eid]]
   {:change-page [:edu.unc.applab.clem.routes/error {:id eid}]
    :db (assoc-in (:db cofx) [:error 0 :message-explanation] expl)}))

(rf/reg-sub
 ::error-sorter
 (fn [db _]
   (:error-sorter db)))

(rf/reg-event-db
 ::set-error-sorter
 (fn [db [_ sorter]]
   (assoc db :error-sorter sorter)))
