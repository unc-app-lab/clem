(ns edu.unc.applab.clem.frontend-spec
  (:require [clojure.spec.alpha :as s]
            [edu.unc.applab.clem.spec :as clms]))

(s/def ::error
  (s/tuple ::clms/exception (s/coll-of (s/and clms/long? pos?))))
(s/def ::errors
  (s/coll-of ::clms/exception))
(s/def ::filtered-errors
  (s/coll-of ::clms/exception))

(s/def ::view fn?)
(s/def ::name keyword?)
(s/def ::event fn?)
(s/def ::parameters map?)
(s/def ::current-page
  (s/keys :req-un [::view ::name]
          :opt-un [::parameters ::event]))
(s/def ::occurrences (s/map-of nat-int? nat-int?))

(s/def ::db
  (s/keys :opt-un [::error ::errors ::filtered-errors ::current-page ::occurrences]))
