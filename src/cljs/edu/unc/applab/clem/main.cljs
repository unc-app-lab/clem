(ns edu.unc.applab.clem.main
  (:require
   [ajax.core :as ajax]
   [reagent.core :as reagent]
   [re-frame.core :as re-frame]
   [reitit.frontend :as reitit]
   [reitit.frontend.easy :as rfe]
   [reitit.coercion.spec :as rss]
   [reitit.coercion :as coercion]
   [edu.unc.applab.clem.events :as evts]
   [edu.unc.applab.clem.routes :as routes]
   [edu.unc.applab.clem.views :as views]))

(defn ^:dev/after-load mount-root [config]
  (re-frame/clear-subscription-cache!)
  (re-frame/dispatch [::evts/initial-load config])
  (rfe/start!
   (reitit/router routes/routes {:compile coercion/compile-request-coercers})
   (fn [m]
     (if (= (:type m) :event)
       (re-frame/dispatch (:event m))
       (re-frame/dispatch [::evts/change-current-page (assoc (:data m)
                                                             :parameters (coercion/coerce! m))])))
   ;; I believe this makes browser history work
   {:use-fragment true})
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn main! []
  (ajax/GET "/config.edn"
            :handler #(mount-root %)
            :error-handler #(mount-root {})
            :response-format evts/edn-response-format))
