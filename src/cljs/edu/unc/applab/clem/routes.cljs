(ns edu.unc.applab.clem.routes
  (:require [edu.unc.applab.clem.views :as views]
            [reitit.coercion.spec :as rss]))

(def routes
  [["/"
    {:name ::list-errors
     :view views/list-errors}]
   ["/errors/:page"
    {:name ::list-errors-page
     :view views/list-errors
     :event :edu.unc.applab.clem.events/change-page
     :coercion rss/coercion
     :parameters {:path {:page int?}}}]
   ["/error/:id"
    {:name ::error
     :view views/error
     :coercion rss/coercion
     :event :edu.unc.applab.clem.events/load-error
     :parameters {:path {:id string?}}}]
   ["/error/:id/edit"
    {:name ::edit-error
     :view views/edit-error
     :coercion rss/coercion
     :parameters {:path {:id string?}}}]])
