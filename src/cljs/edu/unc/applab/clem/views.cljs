(ns edu.unc.applab.clem.views
  (:require
   [re-frame.core :as rf]
   [reagent.core :as r]
   [reitit.frontend.easy :as rfe]
   [cljs-time.format :as f]
   [cljs-time.coerce :as ct]
   [goog.string :as string]
   [edu.unc.applab.clem.events :as evts]))


(set! *warn-on-infer* false) ;; This was getting some type inference errors I wasn't sure how to handle if this wasn't here

(defn- list-errors-row [index element]
  (r/with-let [occurrences (rf/subscribe [::evts/occurrences (:id element)])]
    [:tr
     [:td [:a {:href (rfe/href :edu.unc.applab.clem.routes/error {:id (:id element)})} (:id element)]]
     [:td {:style {:text-align "center"}}
      (if (:message-explanation element)
        [:i.fas.fa-check-circle.yes]
        [:i.fas.fa-times-circle.no])]
     [:td (:message element)]
     [:td (:clojure-version element)]
     [:td (f/unparse (f/formatter "MM/DD/YYYY") (ct/from-long (:timestamp element)))]
     [:td (or @occurrences "#")]]))

(defn- sorter-swap-fn
  ([sort-key sort-fn]
   (fn [{:keys [btn sorter-fn asc]}]
     (if (= btn sort-key)
       (if asc
         {:btn sort-key
          :sorter-fn #(reverse (sort-fn %))
          :asc (not asc)}
         {:btn nil :sorter-fn identity :asc true})
       {:btn sort-key
        :sorter-fn sort-fn
        :asc true})))
  ([sort-key]
   (sorter-swap-fn sort-key #(sort-by sort-key %))))

(defn sorter-btn
  [{current-key :btn} sort-key on-click text]
  [:button
   {:class (str "secondary sorter "
                (if (= current-key sort-key)
                  "selected"))
    :onClick on-click}
   text])

(defn list-errors [{:keys [page]}]
  (r/with-let [error-sorter (r/atom {:btn nil :sorter-fn identity :asc true})
               error-filter (r/atom {})
               errors (rf/subscribe [::evts/errors])
               versions (rf/subscribe [::evts/versions])
               total-page (rf/subscribe [::evts/page-count])
               current-page (rf/subscribe [::evts/page])
               occurrence-list (rf/subscribe [::evts/fetch-all-occurences])
               count-sorter (partial sort
                                     (fn [error1 error2]
                                       (if (map? @occurrence-list)
                                         (compare (get @occurrence-list (:id error1))
                                                  (get @occurrence-list (:id error2)))
                                         0)))]
    [:div.top
     [:script {:src "/list_errors.js" :type "text/javascript"}]
     [:div.title
      [:h1 "All Known Errors"]
      [:div.controls

       [:div.sort-controls
        [:div#sortbyLabel.control-label "Sort By:"]
        [sorter-btn
         @error-sorter
         :message
         (fn [e]
           (r/rswap! error-sorter (sorter-swap-fn :message)))
         "Message"]
        [sorter-btn
         @error-sorter
         :count
         (fn [e]
           (r/rswap! error-sorter (sorter-swap-fn :count count-sorter)))
         "Count"]
        [sorter-btn
         @error-sorter
         :timestamp
         (fn [e]
           (r/rswap! error-sorter (sorter-swap-fn :timestamp)))
         "Date"]]

       [:div.filter-controls
        [:div.control-label "Filter By:"]
        [:button
         {:class (str "secondary "
                      (when (contains? @error-filter :explanation)
                        "selected"))
          :onClick (fn [e]
                     (r/rswap! error-filter (fn [current]
                                              (if (contains? current :explanation)
                                                (dissoc current :explanation)
                                                (-> current
                                                    (dissoc :no-explanation)
                                                    (assoc :explanation #(filter :message-explanation %)))))))}
         "Explanation"]
        [:button.secondary
         {:class (str "secondary "
                      (when (contains? @error-filter :no-explanation)
                        "selected"))
          :onClick (fn [e]
                     (r/rswap! error-filter (fn [current]
                                              (if (contains? current :no-explanation)
                                                (dissoc current :no-explanation)
                                                (-> current
                                                    (dissoc :explanation)
                                                    (assoc :no-explanation #(filter (fn [val] (not (:message-explanation val))) %)))))))}
         "No Explanation"]
        [:select#version-filter {:default ""
                                 :onChange (fn [e]
                                             (println (type (-> e .-target .-selectedIndex)))
                                             (let [target (.-target e)
                                                   version (.. e -target -value)]
                                               (r/rswap! error-filter
                                                         (fn [current]
                                                           (if (= "" version)
                                                             (dissoc current :version)
                                                             (assoc current :version #(filter (fn [val] (= (:clojure-version val) version)) %)))))))}
         (concat

          [[:option {:key -1 :value ""} "Clojure Version"]]
          (map-indexed (fn [index v]
                         [:option {:key index} v])
                       @versions))]]]]



     (if (seq? @errors)
       [:table#error-table
        [:thead
         [:tr#header-row
          [:th "ID"]
          [:th "Explanation"]
          [:th#message-th "Error Message"]
          [:th "Version"]
          [:th#date-th "Date Created"]
          [:th#count-th "#"]]]
        (apply vector :tbody (map-indexed
                              (fn [index element]
                                [list-errors-row index element])
                              ((apply comp (:sorter-fn @error-sorter) (vals @error-filter)) @errors)))]
       [:h1 @errors])
     [:div.bottom-nav
      [:button.secondary
       {:on-click #(rf/dispatch [::evts/change-error-page 1])
        :disabled (if (= @current-page 1) true false)}
       "<<="]
      [:button.secondary
       {:on-click #(rf/dispatch [::evts/change-error-page (dec @current-page)])
        :disabled (if (= @current-page 1) true false)}
       "<="]
      [:p "Current Page: " @current-page]
      [:button.secondary
       {:on-click #(rf/dispatch [::evts/change-error-page (inc @current-page)])
        :disabled (if (= @current-page @total-page) true false)}
       "=>"]
      [:button.secondary
       {:on-click #(rf/dispatch [::evts/change-error-page @total-page])
        :disabled (if (= @current-page @total-page) true false)}
       "=>>"]]]))

(defn- explanation-block [error signed-in]
  (if (and (:message-explanation error)
           (not-empty (:message-explanation error)))
    [:div.explanation
     (when signed-in
       [:a.edit {:href (rfe/href :edu.unc.applab.clem.routes/edit-error {:id (:id error)})} "edit"])
     [:h3 "Explanation"]
     [:pre.explanation-text (:message-explanation error)]]

    [:div.explanation
     [:h3 "Explanation"]
     (if signed-in
       [:a {:href (rfe/href :edu.unc.applab.clem.routes/edit-error {:id (:id error)})}
        [:button.primary "Create"]]
       [:p "No explanation added."])]))

(defn- stack-trace-element-row [index element]
[:tr {:key index}
 [:td index]
 [:td (:class element)]
 [:td (:method element)]
 [:td (:file element)]
 [:td (:line element)]])

(defn- create-error-link [eid]
[:a {:href (rfe/href :edu.unc.applab.clem.routes/error {:id eid})} (str "#" eid)])

(defn vec-join [seperator vec]
(concat (interleave vec (repeat (dec (count vec)) seperator))
        [(last vec)]))

(defn error-header [error causes]
[:<>
 [:div.error-data
  [:h1 (str "Error #" (:id error "unknown"))]
  [:div.error-metadata
   [:dl
    [:dt "Created:"]
    [:dd
     (str (f/unparse
           (f/formatter "YYYY-MM-dd hh:mm")
           (ct/from-long (:timestamp error)))
          " from Clojure v"
          (:clojure-version error))]
    [:dt "Class:"]
    [:dd (:class error)]
    [:dt "Caused by:"]
    [:dd (if-let [caused-by (or (:caused-by-id error)
                                (:id (:caused-by error)))]
           (create-error-link caused-by)
           "none")]
    [:dt "Known to cause: "]
    (if-not (empty? causes)
      (vec (concat
            [:dd]
            (vec-join ", " (map create-error-link causes))))
      "none")]]]
 [:div.error-message (:message error)]])

(defn error [{:keys [id]}]
  (r/with-let [error-info (rf/subscribe [::evts/error])
               identity (rf/subscribe [::evts/identity])]
  (if (or (not @error-info) (string? @error-info))
    [:h1 "Loading..."]
    (let [[error causes] @error-info]
      [:div.fixed-width-container
       [error-header error causes]
       [explanation-block error @identity]
       [:div.stack-trace
        [:h3 "Stack trace"]
        [:table
         [:thead
          [:tr
           [:th "#"]
           [:th "class"]
           [:th "method"]
           [:th "file"]
           [:th "line"]]]
         [:tbody (map-indexed stack-trace-element-row (:stack-trace error))]]]]))))

(defn edit-error [{:keys [id]}]
(r/with-let [error-info (rf/subscribe [::evts/error])]
  (if (and @error-info (= (:id (first @error-info)) id))
    (if (string? @error-info)
      [:h1 @error-info]
      (let [[error causes] @error-info
            expl (r/atom (:message-explanation error))]
        [:div.fixed-width-container
         [error-header error causes]
         [:div.explanation
          [:h3 "Explanation"]
          [:div
           [:textarea.explanation-textarea
            {:name "explanation-string"
             :on-change #(reset! expl (-> % .-target .-value))
             :default (:message-explanation error)}]
           [:a {:href (rfe/href :edu.unc.applab.clem.routes/error {:id id})}
            [:button.edit "Cancel"]]
           [:input.primary {:style {:float "right" :margin "0 1rem"}
                            :type "button"
                            :on-click (fn [e]
                                        (rf/dispatch
                                         [::evts/upsert-explanation @expl id])
                                        (rfe/push-state :edu.unc.applab.clem.routes/error {:id id}))
                            :value "Update"}]]]]))
    (do
      (rf/dispatch [::evts/load-error {:id id}])
      [:h1 "Loading..."]))))

(defn main-panel []
  (r/with-let [current-page (rf/subscribe [::evts/current-page])
               identity (rf/subscribe [::evts/identity])]
    (if-let [evt (:event @current-page)]
      (rf/dispatch [evt (apply merge (vals (:parameters @current-page)))]))
    [:div
     [:header
      [:div.fixed-width-header
       [:a {:href "/#"}
        [:img {:src "/logo.png", :width 41, :height 40}]]
       #_[:input.site-search {:type "text"
                            :placeholder "search…"}]
       [:nav
        [:a {:href "#"} "errors"]]
       (if @identity
         ;; This is a bit of a hack, but it lets us access the api-url config in the db
         ;; This isn't in the with-let at the top because it needs to regenerate everytime
         ;; the current url changes, which doesn't happen using r/with-let due to fragments
         [:a {:href @(rf/subscribe [::evts/generate-api-url
                                    (str "signout?redirect="
                                         (string/urlEncode (.. js/window -location -href)))])} (:username @identity)]
         [:a {:href @(rf/subscribe [::evts/generate-api-url
                                    (str "signin?redirect="
                                         (string/urlEncode (.. js/window -location -href)))])} [:img {:src "/user-icon.png", :width 41, :height 40}]])]]
     [:div.fixed-width-container
      (if @current-page
        [(:view @current-page) (apply merge (vals (:parameters @current-page)))]
        [:h1 "Loading..."])]]))
